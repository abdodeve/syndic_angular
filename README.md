# E-Syndic SaaS | Front-End (Angular 8)

Web application - Front-End for Condo mangement 

Single page application by Angular 8, Consume Laravel api

## Getting Started

E-Syndic is a web application for online condominium management in SAAS mode (software as a service) integrating the accounting, the extranet and the module of the general assembly. With our software, you can manage your homes wherever you are, just have your PC/Mobile and internet access.

### Requirements

* For Perfect environement, I recommended to use Vagrant/homstead-box

* Else be sure you have these requirements :

* npm: 8.1.0
* Node: 12.3.1
* Angular CLI: 8.0.1
* Angular: 8.0.0
* OS: win32 x64 / linux

### Prerequisites & Configuration

* Be sure you have an api-server(syndic-saas backend) configured and running
* Get server domaine ```ex: http://192.168.33.10:8000```
* Point the application on the api-server by edit **api_url (app/service/globale.service, line 99)** 
* Run ```# ng serve -o``` to check if everything is ok !


### Installing


Then Run these commands:

```
# install npm
```

```
# install node
```

```
# After installation add this packages to node_modules (They have specific adjusts for Angular 8)
# URL: https://drive.google.com/file/d/19PLlSpeQDCtByaV1AEQxnrtuORkFTK52/view?usp=sharing
```


## Running the tests

* Run in terminal ```# ng serve -o```
 
## Deployment

* You would need just a web server to deploying on any OS (apache/nginx etc ...)
* Use git or filezilla ..

## Built With

* [Angular 8](https://angular.io) - Javascript framework
* [Node](https://nodejs.org) - web server
* [npm](https://www.npmjs.com) - package manager for JavaScript
* [Bootstrap 4](getbootstrap.com) - Toolkit for developing with HTML, CSS, and JS

## Contributing

Please read for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [E-syndic 1.0.0](https://syndic-beta.marocgeek.com) for versioning. For the versions available,

## Authors

* **Abdelhadi Habchi** - *Initial work & FullStack developer* - [AbdelhadiDev](https://abdelhadidev.com)

## License

This project is licensed under the MIT License