import { Injectable } from '@angular/core';
import { BehaviorSubject ,  Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgxPermissionsService } from 'ngx-permissions';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';


declare const Echo: any;
declare const Pusher: any;

export class GlobalObject {
  title?: string ;
  breadcrumb?: any[] ;
  isLoggedIn?: boolean ;
  api_url?: string ;
  access_token?: string ;
  refresh_token?: string ;
  client_id?: string ;
  client_secret?: string ;
  configNotify?: object ;
  localeText?: any ;
  userLoggedIn?: any ;
  user_id?: any ;
  side?: any ;
  timer?: any ;
  lastActive?: any ;

  // GlobaleObject Consctructor
  constructor() {
  }
}

@Injectable()
export class GlobaleService {

  private localeText = {
    // for filter panel
    page: 'Page',
    more: 'Plus',
    to: 'à',
    of: 'de',
    next: 'Suivant',
    last: 'Dérnier',
    first: 'Premier',
    previous: 'Précédent',
    loadingOoo: 'Chargement...',

    // for set filter
    selectAll: 'Séléctionné tous',
    searchOoo: 'Chercher...',
    blanks: 'Vide',

    // for number filter and text filter
    filterOoo: 'Filtre...',
    applyFilter: 'Appliquer filtre...',

    // for number filter
    equals: 'Égal',
    notEqual: 'Pas égal',
    lessThan: 'Moins que',
    greaterThan: 'Plus que',

    // for text filter
    contains: 'Contients',
    notContains: 'Pas contients',
    startsWith: 'Commence par',
    endsWith: 'Termine par'
   };

   /*
   * Observable GlobalObject
   */

// MarocGeek Clients                                                //
// api_url: https://marocgeek.com/syndic-beta/backend/public        //
// client_id: '2' ,                                                 //
// client_secret: 'vQRhokWk7VzlwYMPZEhTWi9JSkXvxlD4hiyWh1hw' ,      //
//                                                                  //
// Local Clients                                                    //
// api_url: http://127.0.0.1:8000                                   //
// client_id: '2' ,                                                 //
// client_secret:  'vQRhokWk7VzlwYMPZEhTWi9JSkXvxlD4hiyWh1hw' ,     //
//                                                                  //
// Vagrant                                                          //
// api_url: http://192.168.33.10:8000                               //
// client_id: '2' ,                                                 //
// client_secret:  'vQRhokWk7VzlwYMPZEhTWi9JSkXvxlD4hiyWh1hw' ,     //
///////////////////////////////////////////////////////////////////////

  isLoggedIn = null; // localStorage.getItem('isLoggedIn') ? JSON.parse(localStorage.getItem('isLoggedIn')) : null ;
  access_token = null; //  localStorage.getItem('access_token') ? JSON.parse(localStorage.getItem('access_token')) : null;
  userLoggedIn = null; //  localStorage.getItem('userLoggedIn') ? JSON.parse(localStorage.getItem('userLoggedIn')) : null;
  user_id = null; //  localStorage.getItem('user_id') ? JSON.parse(localStorage.getItem('user_id')) : null;

   globalObjectDefault: GlobalObject = {
    title: 'title is empty',
    breadcrumb: ['bradcrumb is empty'],
    api_url: 'http://127.0.0.1:8000' ,
    client_id: '6' ,
    client_secret: 'QPeTTF01UUtGSnpBdDfKtEdeCHvSfE4rnHdcRphE' ,
    isLoggedIn: (localStorage.getItem('isLoggedIn') != 'undefined' && localStorage.getItem('isLoggedIn') != null ) ? JSON.parse(localStorage.getItem('isLoggedIn')) : null,
    access_token:  (localStorage.getItem('access_token') != 'undefined' && localStorage.getItem('access_token') != null ) ? JSON.parse(localStorage.getItem('access_token')) : null,
    refresh_token: (localStorage.getItem('refresh_token') != 'undefined' && localStorage.getItem('refresh_token') != null ) ? JSON.parse(localStorage.getItem('refresh_token')) : null,
    userLoggedIn: (localStorage.getItem('userLoggedIn') != 'undefined' && localStorage.getItem('userLoggedIn') != null ) ? JSON.parse(localStorage.getItem('userLoggedIn')) : null,
    user_id: (localStorage.getItem('user_id') != 'undefined' && localStorage.getItem('user_id') != null ) ? JSON.parse(localStorage.getItem('user_id')) : null,
    configNotify: {'position': 'rightTop', 'timeout': '4000'},
    localeText: this.localeText,
    side: 'home'
  } ;
  globalObject = new BehaviorSubject(this.globalObjectDefault);
  msgSynchronize = new BehaviorSubject(null);
  coproprieteExercice = new BehaviorSubject(null) ;

  // GlobalService Constructor
  constructor(private http: HttpClient,
              private permissionsService: NgxPermissionsService,
              private spinnerService: Ng4LoadingSpinnerService) {
  }

   // Broadcast GlobalObject
   get_Observable_GlobalObject(): Observable < GlobalObject > {
     return this.globalObject ;
   }
  // Set globalObject
  setGlobalObject(globalObject: GlobalObject) {
    this.globalObject.next(globalObject);
  }

  // Broadcast MsgSynchronize
  get_Observable_MsgSynchronize(): Observable < any > {
    return this.msgSynchronize ;
  }
  // Set MsgSynchronize
  setMsgSynchronize(message: any) {
    this.msgSynchronize.next(message);
  }

  // Broadcast CoproprieteExercice
  get_Observable_CoproprieteExercice(): Observable < any > {
    return this.coproprieteExercice ;
  }
  // Set CoproprieteExercice
  setCoproprieteExercice(data: any) {
    this.coproprieteExercice.next(data);
  }

  // Set Auth localStorage
  setLocalAuth() {
        localStorage.setItem('access_token', JSON.stringify(this.globalObject.value.access_token)) ;
        localStorage.setItem('refresh_token', JSON.stringify(this.globalObject.value.refresh_token)) ;
        localStorage.setItem('isLoggedIn', JSON.stringify(this.globalObject.value.isLoggedIn)) ;
  }

  // Unset Auth from localStorage
  unsetLocalAuth() {

        localStorage.setItem('access_token', JSON.stringify(null)) ;
        localStorage.setItem('refresh_token', JSON.stringify(null)) ;
        localStorage.setItem('isLoggedIn', JSON.stringify(false)) ;
        localStorage.setItem('userLoggedIn', JSON.stringify(null)) ;
        localStorage.setItem('user_id', JSON.stringify(null)) ;
        localStorage.setItem('permissions', JSON.stringify(null)) ;

        this.globalObject.value.access_token  = null ;
        this.globalObject.value.refresh_token = null ;
        this.globalObject.value.isLoggedIn    = false ;
        this.globalObject.value.userLoggedIn  = null ;
        this.globalObject.value.user_id  = null ;
  }

  listen () {
      // Pusher-JS
      const p = new Pusher('617c452e406a7e0d19e5', {
        cluster: 'eu',
        encrypted: true
      });

      let channel = p.subscribe('adevChannel');
      channel.bind('EventGlobale', (data) => {
        this.setMsgSynchronize(data);
      });
  }
}
