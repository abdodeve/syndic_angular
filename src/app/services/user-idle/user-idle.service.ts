import { Injectable, OnDestroy } from '@angular/core';
import { GlobaleService, GlobalObject } from '../../services/globale.service';
import { Observable } from 'rxjs' ;
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, retry } from 'rxjs/operators';
// import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Router } from '@angular/router';
import { LoginServiceService } from '../../components/login/service/login-service.service' ;



@Injectable({
  providedIn: 'root'
})
export class UserIdleService implements OnDestroy {

  /*
  * Syncronous variable
  */
 private globalObject: GlobalObject ;
 private globalObjectSubscription ;

 private minutesAllowed = 10 ;

 constructor(private http: HttpClient, 
             private globaleService: GlobaleService,
             private spinnerService: Ng4LoadingSpinnerService,
             private router: Router,
             private loginService: LoginServiceService,
             ) {
  // Subscribe to globaleService
  this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject()
                     .subscribe(globalObject => this.globalObject = globalObject) ;
}


  ngOnDestroy () {
    this.globalObjectSubscription.unsubscribe();
  }

  
  /**
   * Inactivity - IDLE
   * 
   * 
   * @returns void
   */
    CheckInactivity() {
      if (!document.hasFocus()) 
          return ;

      let lastActive = JSON.parse(localStorage.getItem('lastActive')) ;
      if( !lastActive ) {
        localStorage.setItem('lastActive', JSON.stringify(new Date().getTime()));
        return ;
      }
  
      let now = new Date().getTime();
  
      // If Token Expired Clear Interval
      if (!this.globalObject.isLoggedIn) {
        clearInterval(this.globalObject.timer);
        localStorage.removeItem('lastActive');
        return ;
      }
   
      // Minutes between now and lastActive
      let diffMinute = (now - lastActive)/(1000*60) ;

      // If 10 Minute of Inactivity LogOut
      if (diffMinute > this.minutesAllowed) {
        // Logout Code
        var lastActTest = new Date(JSON.parse(localStorage.getItem('lastActive')));
        console.log('LogOut Time', lastActTest);
        this.loginService.logoutAction();
        return ;
      }
  
        // Else Renew last Active
        localStorage.setItem('lastActive', JSON.stringify(new Date().getTime()));
    }

  /**
   * startWatching: Start watching user activity
   *  
   * @returns void
   */
  startWatching(){
      // Launch CheckInactivity Interval
      let timer = setInterval(() => {
                                      console.log("setInterval");
                                      this.CheckInactivity();
                                    },
                                    1 * 1 * 1000 
                            );

    // Set timer to GlobalObject
    this.globaleService.setGlobalObject({...this.globalObject, ...{'timer': timer}});
  }
  
}



