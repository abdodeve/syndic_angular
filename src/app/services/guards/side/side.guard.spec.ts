import { TestBed, async, inject } from '@angular/core/testing';

import { SideGuard } from './side.guard';

describe('SideGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SideGuard]
    });
  });

  it('should ...', inject([SideGuard], (guard: SideGuard) => {
    expect(guard).toBeTruthy();
  }));
});
