import { Injectable, OnDestroy } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import {Router, ActivatedRoute} from '@angular/router';
import { GlobaleService, GlobalObject } from './../../../services/globale.service';

@Injectable()
export class SideGuard implements CanActivate, OnDestroy {

  /*
  * Globale variable
  */
 private globalObject: GlobalObject = new GlobalObject() ;
 private globalObjectSubscription ;

  constructor(private globaleService: GlobaleService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
               // Subscribe to globaleService
               this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject()
                                    .subscribe(globalObject => this.globalObject = globalObject) ;
}

ngOnDestroy () {
  this.globalObjectSubscription.unsubscribe();
}

canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    // console.log(next.routeConfig.path);
    const globalObjectNew: GlobalObject = {
      'side': next.routeConfig.path
     } ;
    const mergeObjects = {...this.globalObject, ...globalObjectNew} ;
    this.globaleService.setGlobalObject(mergeObjects) ; 

    return true ;
  }
}
