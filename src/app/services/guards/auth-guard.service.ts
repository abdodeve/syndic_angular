import { Injectable, OnDestroy } from '@angular/core';
import { Router, CanActivate, CanActivateChild } from '@angular/router';
import { GlobaleService, GlobalObject } from '../../services/globale.service';

@Injectable()
export class AuthGuardService  implements CanActivate, CanActivateChild, OnDestroy {

  /*
  * Globale variable
  */
 globalObject: GlobalObject = new GlobalObject() ;

 private globalObjectSubscription ;

 constructor(private globaleService: GlobaleService, private router: Router) {
            this.globalObjectSubscription = this.globaleService.
                 get_Observable_GlobalObject().subscribe(res => this.globalObject = res) ;
 }

  canActivate() {
    if (this.globalObject.isLoggedIn === false) {
        console.log('You are logged out - I Redirected you to Login');
        this.globaleService.unsetLocalAuth();
        this.router.navigate(['/login']);
        return false;
      }
    return true ;
  }

  canActivateChild() {
    console.log('checking child route access');
    return true;
  }

  ngOnDestroy () {
    this.globalObjectSubscription.unsubscribe();
  }

}
