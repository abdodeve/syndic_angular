import { Injectable, OnDestroy } from '@angular/core';
import { GlobaleService, GlobalObject } from '../services/globale.service';
import {SnotifyService, SnotifyPosition, SnotifyToastConfig} from 'ng-snotify';

@Injectable({
  providedIn: 'root'
})

export class GridClass implements OnDestroy {

  private globalObject: GlobalObject = new GlobalObject() ;
  private globalObjectSubscription ;

    constructor (private snotifyService: SnotifyService,
                 private globaleService: GlobaleService) {
      // Subscribe to globaleService (globalObject)
      this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject()
      .subscribe(globalObject => this.globalObject = globalObject) ;
      this.setGlobalObject() ;
    }

    ngOnDestroy () {
      this.globalObjectSubscription.unsubscribe();
    }

      // Set GlobalObject
  setGlobalObject(): void {
    const globalObjectNew: GlobalObject = {
                                           'title': 'Liste utilisateur',
                                           'breadcrumb': ['Utilisateur', 'Liste']
                                          } ;
    const mergeObjects = {...this.globalObject, ...globalObjectNew} ;
    this.globaleService.setGlobalObject(mergeObjects);
  }

    insert (gridApi, insertedRow) {
      if (gridApi) {
        gridApi.updateRowData({ add: insertedRow, addIndex: 0 });
      }
    }

    update (gridApi, updatedRow) {
      if (gridApi) {
        const rowNode = gridApi.getRowNode(updatedRow.id);
        if (rowNode) {
          const newData = {...rowNode.data, ...updatedRow} ;
          gridApi.updateRowData({ update: [newData]});
        }
      }
    }

    delete (gridApi, deleted_id) {
        if (gridApi) {
          const rowNode = gridApi.getRowNode(deleted_id);
          gridApi.updateRowData({ remove: [rowNode] });
        }
      }

      deleteMultiple (gridApi, ids) {
        if (gridApi) {
          const deleted_ids: any[] = ids ;
          const rowNodes: any[] = [] ;
          deleted_ids.forEach(element => {
            const rowNode = gridApi.getRowNode(element);
            rowNodes.push(rowNode);
          });
          gridApi.updateRowData({ remove: rowNodes });
        }
      }

  deleteFromGrid(gridApi, ids, model){
        //Delete from Grid
        this.deleteMultiple(gridApi, ids);
        let msg = `${model.singular}: ${ids} est supprimé` ;
        if (ids.length > 1) 
          msg = `${model.plural}: ${ids} sont supprimé` ;
        this.snotifyService.info(msg, this.globalObject.configNotify);
  }

  insertToGrid(gridApi, insertedObject,  model, showNotify = true) {
    this.insert(gridApi, [insertedObject]);
    if(showNotify){
        this.snotifyService.success(
                                    `${model.singular}: ${insertedObject.id} ajouté`,
                                    this.globalObject.configNotify
                                  );
    }
  }

  updateGrid(gridApi, update, model) {
    this.update(gridApi, update);
    this.snotifyService.info(
                              `${model.singular}: ${update.id} modifié`,
                              this.globalObject.configNotify
                            );
  }

  restoreGrid(gridApi, restoredObjects, restored_id, model){
    this.insert(gridApi, restoredObjects);

    let msg = `${model.singular}: ${restored_id} est réstauré` ;
    if (restoredObjects.length > 1) {
      msg = `${model.plural}: ${restored_id} sont réstauré` ;
    }
    this.snotifyService.info(msg, this.globalObject.configNotify);
  }

  importToGrid(gridApi, importedObjects, showNotify = true) {
    this.insert(gridApi, importedObjects);
    if(showNotify){
        this.snotifyService.success(
                                    `Liste des proprietaires est importé avec succés`,
                                    this.globalObject.configNotify
                                  );
    }
  }

}
