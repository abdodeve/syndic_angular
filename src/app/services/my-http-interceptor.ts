
import {throwError as observableThrowError,  Observable } from 'rxjs';

import {catchError} from 'rxjs/operators';
import { Injectable, Injector, OnDestroy } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';


import { Router } from '@angular/router';
import { GlobaleService, GlobalObject } from './globale.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {SnotifyService, SnotifyPosition, SnotifyToastConfig} from 'ng-snotify';
import { LoginServiceService } from '../components/login/service/login-service.service' ;

@Injectable()
export class MyHttpInterceptor implements HttpInterceptor, OnDestroy {

        globalObject: GlobalObject = new GlobalObject() ;
        private globalObjectSubscription ;

        constructor( private globaleService: GlobaleService,
                     private router: Router,
                     private spinnerService: Ng4LoadingSpinnerService,
                     private snotifyService: SnotifyService,
                     private loginServiceService: LoginServiceService ) {
                   // Subscribe to globaleService
                   this.globalObjectSubscription = this.globaleService
                         .get_Observable_GlobalObject()
                         .subscribe(globalObject => this.globalObject = globalObject) ;
         }

         ngOnDestroy () {
           this.globalObjectSubscription.unsubscribe();
         }

        intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

                // Clone the request to add the new header.
                const authReq = req.clone({headers: req.headers.set('Accept', 'application/json')});

                // send the newly created request
                return next.handle(req).pipe(
                catchError((error, caught) => {
                        this.spinnerService.hide();
                        // intercept the respons error and display it to the console
                        console.log('Error Interceptor');
                        console.log(error);
                        switch (error.status) {
                                case 401: {
                                    if (error.error.error !== 'invalid_credentials') {

                                        console.log(this.globalObject);
                                       
                                        // Clean tokens
                                        // this.globaleService.unsetLocalAuth() ;
                                        // console.log(this.globalObject.isLoggedIn) ;

                                        this.snotifyService.error('Session expired. (401)',
                                                                  this.globalObject.configNotify);
                                        if (this.router.url !== '/login' ) {
                                                console.log('You are logged out I Redirect you to LOGIN') ;
                                                this.globaleService.unsetLocalAuth();
                                                 this.router.navigate(['/login']);
                                        }
                                     }
                                        break ;
                                }
                                case 400: {
                                        console.log('Http failure response Bad Request. (400)');
                                        this.snotifyService.error('Http failure response Bad Request. (400)',
                                                                     this.globalObject.configNotify);
                                        break ;
                                }
                                default: {
                                        this.snotifyService.warning('Unexpected error', this.globalObject.configNotify);
                                        // this.globaleService.unsetLocalAuth();
                                        // this.router.navigate(['/login']);
                                        break ;
                                }
                        }
                        // return the error to the method that called it
                        return observableThrowError(error);
                })) as any;
        }
}
