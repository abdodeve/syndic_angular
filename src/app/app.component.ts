import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { GlobaleService, GlobalObject } from './services/globale.service';
import '../assets/js/jquery.app.js';
import {Router, ActivatedRoute} from '@angular/router';
import { Location } from '@angular/common';
import { TopbarService } from './components/topbar/service/topbar.service' ;
import { NgxPermissionsService } from 'ngx-permissions';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { LoginServiceService } from './components/login/service/login-service.service' ;
import { UserIdleService } from './services/user-idle/user-idle.service';



declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {

  title = 'app';
  /*
  * Syncronous variable
  */
  private globalObject: GlobalObject ;
  private globalObjectSubscription ;
  private timer ;
  private lastActive = new Date().getTime() ;


  constructor(private location: Location,
              private globaleService: GlobaleService,
              private userIdleService: UserIdleService,
              ) {
              // Subscribe to globaleService (globalObject)
              this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject()
                                 .subscribe(globalObject => this.globalObject = globalObject) ;
             this.globaleService.listen();
  }

  ngAfterViewInit() {

  }

  ngOnInit() {

    if( this.globalObject.isLoggedIn ) {
      // Start watching user activities
      this.userIdleService.startWatching();
    }

  }

  ngOnDestroy () {
    this.globalObjectSubscription.unsubscribe();
  }

}
