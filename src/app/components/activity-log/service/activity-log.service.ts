import { Injectable, OnInit, OnDestroy } from '@angular/core';
import { GlobaleService, GlobalObject } from '../../../services/globale.service';
import { Observable } from 'rxjs' ;
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ActivityLogService {


  /*
  * Syncronous variable
  */
 private globalObject: GlobalObject = new GlobalObject() ;
 private coproprieteExercice ;
 private globalObjectSubscription ;


 constructor(private http: HttpClient, private globaleService: GlobaleService) {
       // Subscribe to globaleService
       this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject()
       .subscribe(globalObject => this.globalObject = globalObject) ;
  }


 ngOnInit() {

 }

 ngOnDestroy () {
   this.globalObjectSubscription.unsubscribe();
 }

 fetch(): Observable <any> {

   const url             = this.globalObject.api_url + '/api/activity_log/fetch' ;
   const body            = null ;
   const httpOptions = {
                         headers: new HttpHeaders({
                           'Accept'        : 'application/json',
                           'Content-Type'  : 'application/json',
                           'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                         })
                       };
   return this.http.post<any>(url, body, httpOptions) ;
 }

}
