import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute} from '@angular/router';
import { AgGridNg2 } from 'ag-grid-angular';
import { GridApi, ColumnApi, Events } from 'ag-grid';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig} from 'ng-snotify';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { GlobaleService, GlobalObject } from '../../../services/globale.service';
import { ActivityLogService } from '../service/activity-log.service';
import { GridClass } from '../../../services/grid.class' ;
import { NgxPermissionsService  } from 'ngx-permissions' ;

@Component({
  selector: 'app-liste-activity-log',
  templateUrl: './liste-activity-log.component.html',
  styleUrls: ['./liste-activity-log.component.css']
})
export class ListeActivityLogComponent  implements OnInit, OnDestroy {

  /*
  * Globale variables
  */
  private globalObject: GlobalObject = new GlobalObject() ;
  private columnDefs = [
    {headerName: 'Utilisateur', field: 'user_name', checkboxSelection: true },
    {headerName: 'Description', field: 'description' },
    {headerName: 'Table', field: 'table'},
    {headerName: 'Date', field: 'date'},
    {headerName: 'Methode', field: 'methode'},
    {headerName: 'Route', field: 'route'},
    {headerName: 'IP Address', field: 'ip_address'}
  ] ;
  private rowData: any ;
  private gridApi: GridApi ;
  private gridColumnApi: ColumnApi ;
  private isColFit = true ;
  private isRowsSelected = false ;
  private getRowNodeId ;
  private msgSynchronizeSubscription ;
  private globalObjectSubscription ;
  private coproprieteExercice ;
  private coproprieteExerciceSubscription ;
  private model = {'singular': 'Activité', 'plural': 'Activités'} ;


constructor(private globaleService: GlobaleService,
            private http: HttpClient,
            private activityLogService: ActivityLogService,
            private snotifyService: SnotifyService,
            private activatedroute: ActivatedRoute,
            private router: Router,
            private spinnerService: Ng4LoadingSpinnerService,
            private gridClass: GridClass,
            private permissionsService: NgxPermissionsService) {
    // Subscribe to globaleService (globalObject)
    this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject()
                       .subscribe(globalObject => this.globalObject = globalObject) ;
    // Subscribe to coproprieteExercice
    this.coproprieteExerciceSubscription = this.globaleService.get_Observable_CoproprieteExercice()
          .subscribe( res => {
                                 if (!res) { return ; }
                                 this.coproprieteExercice = res ;
                                 this.setDataToGrid();
                              }
                    );
    this.setGlobalObject() ;
    this.getRowNodeId = function(data) {
      if (data) return data.id;
    };
    this.synchronize();
 }

  ngOnInit() {
  }

  ngOnDestroy () {
    this.globalObjectSubscription.unsubscribe();
    this.coproprieteExerciceSubscription.unsubscribe();
    this.msgSynchronizeSubscription.unsubscribe();
  }

  setDataToGrid() {
    this.spinnerService.show();

    this.activityLogService.fetch().subscribe(res => {
      this.spinnerService.hide();
      console.log(res.activities);
      if(res.activities)
        this.rowData = res.activities ;
      else
        this.router.navigate(['/home']);
    });
  }

  onGridReady(params) {
    // Assign grid api to local vars
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    // Fit columns for Fill screen
    this.gridApi.sizeColumnsToFit();
  }

  // Set GlobalObject
  setGlobalObject(): void {
    const globalObjectNew: GlobalObject = {
                                           'title': 'Journal des activités',
                                           'breadcrumb': ['Activités']
                                          } ;
    const mergeObjects = {...this.globalObject, ...globalObjectNew} ;
    this.globaleService.setGlobalObject(mergeObjects);
  }

  // onDoubleRowClicked autoSize/fitSize
  onDoubleRowClicked(event) {
    if (this.isColFit) {
      this.gridColumnApi.autoSizeAllColumns();
      this.isColFit = false ;
    } else {
      this.gridApi.sizeColumnsToFit();
      this.isColFit = true ;
    }
  }

  // Filter Grid onKeyp
  onKeyp(event) {
    this.gridApi.setQuickFilter(event.target.value);
  }

  synchronize() {
    this.msgSynchronizeSubscription = this.globaleService.get_Observable_MsgSynchronize().subscribe(res => {
          if (!res) {
            return ;
          }
          if (!res.message) {
            return ;
          }
          if (!res.message.original) {
            return ;
          }
          if (!res.message.original.activity_log) {
            return ;
          }
          const message = res.message.original.activity_log ;

          // Insert
          if (message.insertedObject) {
                  // Insert to Grid
                  this.gridClass.insertToGrid(this.gridApi, message.insertedObject, this.model, false);
            }

          // clear msg
          if (res.message) {
             this.globaleService.setMsgSynchronize(null) ;
          }
      });
  }

}
