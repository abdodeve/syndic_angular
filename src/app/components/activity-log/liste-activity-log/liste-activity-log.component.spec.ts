import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeActivityLogComponent } from './liste-activity-log.component';

describe('ListeActivityLogComponent', () => {
  let component: ListeActivityLogComponent;
  let fixture: ComponentFixture<ListeActivityLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeActivityLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeActivityLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
