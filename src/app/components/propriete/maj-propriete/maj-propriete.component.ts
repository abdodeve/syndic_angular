import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm, Form } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Router, ActivatedRoute} from '@angular/router';
import {SnotifyService, SnotifyPosition, SnotifyToastConfig} from 'ng-snotify';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { GlobaleService, GlobalObject } from '../../../services/globale.service';
import { Propriete } from '../propriete' ;
import { ProprieteService } from '../service/propriete.service';
import { TopbarService } from '../../topbar/service/topbar.service' ;

@Component({
  selector: 'app-maj-propriete',
  templateUrl: './maj-propriete.component.html',
  styleUrls: ['./maj-propriete.component.css']
})
export class MajProprieteComponent implements OnInit, OnDestroy {

  /*
  * Globale variables
  */
 private globalObject: GlobalObject = new GlobalObject() ;
 private propriete: Propriete = new Propriete() ;
 private id ;
 private isNew = true ;
 private next ;
 private previous ;
 private globalObjectSubscription ;
 private copropriete ;

 constructor(private globaleService: GlobaleService,
             private proprieteService: ProprieteService,
             private http: HttpClient,
             private snotifyService: SnotifyService,
             private activatedroute: ActivatedRoute,
             private router: Router,
             private spinnerService: Ng4LoadingSpinnerService,
             private topbarService: TopbarService
           ) {

    // Subscribe to globaleService (globalObject)
    this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject()
                       .subscribe(globalObject => this.globalObject = globalObject) ;
    this.globaleService.get_Observable_CoproprieteExercice().subscribe(res => {
        if (!res) {
          return ;
        }
        this.copropriete = res ;
    });
  }

  ngOnInit(): void {
    this.NewOrEdit();
    this.setGlobalObject() ;
  }

  ngOnDestroy () {
    this.globalObjectSubscription.unsubscribe();
  }

  /**
   * Set Default GlobalObject
   *  
   * @returns void
   */
  setGlobalObject(): void {
    let globalObjectNew: GlobalObject ;
    // Check if you are on New or Edit
    if (this.isNew) {
          globalObjectNew = {
                              'title': 'Nouveau propriete',
                              'breadcrumb': ['Propriete', 'Nouveau']
                            } ;
    } else {
          globalObjectNew = {
                              'title': 'Editer propriete',
                              'breadcrumb': ['Propriete', 'Editer']
                            } ;
    }
    const mergeObjects = {...this.globalObject, ...globalObjectNew} ;
    this.globaleService.setGlobalObject(mergeObjects);
  }

  /**
   * onValidate: Click Event On Validate Btn
   * 
   * On Validate Click - insert/update
   * 
   * @param NgForm form
   * @returns void
   */
  onValidate(form: NgForm) {

    this.spinnerService.show();
    if (form.invalid) {
      return;
    }
    if ( this.isNew ) {
      this.propriete.fk_copropriete = this.copropriete.copropriete_id ;
      this.proprieteService.insert( this.propriete ).subscribe( data => {
        this.spinnerService.hide();
        this.router.navigate(['/home/propriete/liste']);
      });
    } else {
      this.proprieteService.update( this.id, this.propriete ).subscribe( data => {
        this.spinnerService.hide();
        this.router.navigate(['/home/propriete/liste']);
      });
    }
  }

  /**
   * onDelete: Click event on delete Btn
   * 
   * On Delete Click
   * 
   * @returns void
   */
  onDelete() {
    this.snotifyService.confirm('Êtes-vous sûr de vouloir supprimer ce propriete ?', {
      timeout: 8000,
      position: 'centerTop',
      buttons: [
        {text: 'Oui', action: () => this.deleteConfirmed(this.id), bold: false},
        {text: 'Non', action: (toast) => this.snotifyService.remove(toast.id)},
        {text: 'Fermer', action: (toast) => {this.snotifyService.remove(toast.id); }, bold: true},
      ]
    });
  }

  /**
   * onCancel: Click Event on Cancel Btn
   * 
   * On Cancel Click
   * 
   * @returns void
   */
  onCancel() {
    this.router.navigate(['/home/propriete/liste']);
  }

  
  /**
   * Get Single Propriete
   * 
   * @returns void
   */
  single() {
    this.spinnerService.show();
    this.proprieteService.single(this.id).subscribe( res => {
      this.spinnerService.hide();

      // If id not found
      if(res.error == "not_found")   {
        this.snotifyService.warning('Propriete introuvable !', this.globalObject.configNotify);
        this.router.navigate(['/home/propriete/liste']);
        return ;
      }

      // Is Propriete Exist
      if(res.propriete) {
        this.propriete = res.propriete ;
        this.next = res.next ;
        this.previous = res.previous ;
      } 
      // Is not Exist
      else {
        this.router.navigate(['/home']);
      }
 
      } 
    );
  }

  /**
   * Check mode - If New or Edit
   * 
   * @returns void
   */
  NewOrEdit() {
    this.activatedroute.params.subscribe(params => {
      this.id = params['id'] ;
        if (this.id) {
            this.isNew = false ;
            this.single();
        }
    });
  }

  /**
   * Delete confirmed
   * 
   * @param int id
   * @returns void
   */
  deleteConfirmed(id) {
    this.snotifyService.clear();
    this.spinnerService.show();
    this.proprieteService.delete([id]).subscribe(res => {
          this.spinnerService.hide();
          this.router.navigate(['/home/propriete/liste']);
     });
  }

  /**
   * Check If is the End of propriete (previous/next)
   * 
   * @param String nextOrPrevious
   * @returns void
   */
  isEnd(nextOrPrevious) {
    if (nextOrPrevious === 'next') {
      return this.next == null ? true : false ;
    }
    if (nextOrPrevious === 'previous') {
      return this.previous == null ? true : false ;
    }
  }

}
