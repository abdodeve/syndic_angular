import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MajProprieteComponent } from './maj-propriete.component';

describe('MajProprieteComponent', () => {
  let component: MajProprieteComponent;
  let fixture: ComponentFixture<MajProprieteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MajProprieteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MajProprieteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
