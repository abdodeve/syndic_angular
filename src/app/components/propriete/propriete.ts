export class Propriete {
    id? ;
    fk_copropriete? ;
    num_propriete? ;
    type_copropriete? ;
    batiment? ;
    etage? ;
    num_titre? ;
    surface? ;
    quote_par_terrain? ;
    pt_indivision? ;
    voix? ;
    taux_tantiem? ;
    commentaire? ;
    type_utilisation? ;
    n_tourne? ;
    police_eau? ;
    article_impot? ;
}
