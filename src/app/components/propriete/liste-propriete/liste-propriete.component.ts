import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute} from '@angular/router';
import { AgGridNg2 } from 'ag-grid-angular';
import { GridApi, ColumnApi, Events } from 'ag-grid';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig} from 'ng-snotify';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { GlobaleService, GlobalObject } from '../../../services/globale.service';
import { Propriete } from '../propriete' ;
import { ProprieteService } from '../service/propriete.service';
import { GridClass } from '../../../services/grid.class' ;
import { NgxPermissionsService  } from 'ngx-permissions' ;

declare const Echo: any;
@Component({
  selector: 'app-liste-propriete',
  templateUrl: './liste-propriete.component.html',
  styleUrls: ['./liste-propriete.component.css']
})
export class ListeProprieteComponent implements OnInit, OnDestroy {

  /*
  * Globale variables
  */
  private globalObject: GlobalObject = new GlobalObject() ;
  private columnDefs = [
    {headerName: 'Num propriete', field: 'num_propriete', checkboxSelection: true },
    {headerName: 'Type copropriete', field: 'type_copropriete' },
    {headerName: 'Etage', field: 'etage'},
    {headerName: 'Num titre', field: 'num_titre'},
    {headerName: 'Surface', field: 'surface'},
    {
      headerName: 'Action',
      suppressMenu: true,
      suppressSorting: true,
      width: 130,
      template: `
                <button type="button" data-action-type="edit"
                  class="btn btn-icon waves-effect waves-light btn-info" title="Editer">
                  <i class="fa fa-edit" data-action-type="edit"></i>
                </button>

                <button type="button" data-action-type="delete"
                  class="btn btn-icon waves-effect waves-light btn-danger" title="Supprimer">
                  <i class="fa fa-trash" data-action-type="delete"></i>
                </button>
                `
    }
  ];
  private rowData: any ;
  private gridApi: GridApi ;
  private gridColumnApi: ColumnApi ;
  private isColFit = true ;
  private isRowsSelected = false ;
  private getRowNodeId ;
  private msgSynchronizeSubscription ;
  private globalObjectSubscription ;
  private coproprieteExercice ;
  private coproprieteExerciceSubscription ;
  private model = {'singular': 'Propriete', 'plural': 'Proprietés'} ;


constructor(private globaleService: GlobaleService,
            private http: HttpClient,
            private proprieteService: ProprieteService,
            private snotifyService: SnotifyService,
            private activatedroute: ActivatedRoute,
            private router: Router,
            private spinnerService: Ng4LoadingSpinnerService,
            private gridClass: GridClass,
            private permissionsService: NgxPermissionsService) {
    // Subscribe to globaleService (globalObject)
    this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject()
                       .subscribe(globalObject => this.globalObject = globalObject) ;
    // Subscribe to coproprieteExercice
    this.coproprieteExerciceSubscription = this.globaleService.get_Observable_CoproprieteExercice()
          .subscribe( res => {
                                 if (!res) { return ; }
                                 this.coproprieteExercice = res ;
                                 this.setDataToGrid(this.coproprieteExercice.copropriete_id);
                              }
                    );
    this.setGlobalObject() ;
    this.getRowNodeId = function(data) {
      if (data) return data.id;
    };
    this.synchronize();
 }

  ngOnInit() {
  }

  ngOnDestroy () {
    this.globalObjectSubscription.unsubscribe();
    this.coproprieteExerciceSubscription.unsubscribe();
    this.msgSynchronizeSubscription.unsubscribe();
  }
  
  /**
   * Set data to Grid
   * 
   * @param int id_copropriete
   * @returns void
   */
  setDataToGrid( id_copropriete = null ) {
    this.spinnerService.show();
    this.proprieteService.fetch(id_copropriete).subscribe(res => {
      this.spinnerService.hide();
      if(res.proprietes)
        this.rowData = res.proprietes ;
      else
        this.router.navigate(['/home']);
    });
  }
 
  /**
   * On grid ready
   * Init this.gridApi
   * Init this.gridColumnApi
   * 
   * @param any params
   * @returns void
   */
  onGridReady(params) {
    // Assign grid api to local vars
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    // Fit columns for Fill screen
    this.gridApi.sizeColumnsToFit();
  }

  /**
   * Set Default GlobalObject
   *  
   * @returns void
   */
  setGlobalObject(): void {
    const globalObjectNew: GlobalObject = {
                                           'title': 'Liste propriete',
                                           'breadcrumb': ['Propriete', 'Liste']
                                          } ;
    const mergeObjects = {...this.globalObject, ...globalObjectNew} ;
    this.globaleService.setGlobalObject(mergeObjects);
  }

  /**
   * onRowSelected: Click Event Select Grid
   * 
   * If more than 1 row selected
   * 
   * Set this.isRowsSelected to true
   * 
   * For Display btn 'Supprimer sélections'
   * 
   * @param any params
   * @returns void
   */
  onRowSelected(event) {
    const selectedRows = this.gridApi.getSelectedRows();
    if (selectedRows.length > 1) {
      this.isRowsSelected = true ;
    } else {
      this.isRowsSelected = false ;
    }
  }
  
  /**
   * onDoubleRowClicked: Double Click Event On GridRow
   * 
   * Double Click Grid Event
   * 
   * autoSize/fitSize Grid
   * 
   * @param any event
   * @returns void
   */
  onDoubleRowClicked(event) {
    if (this.isColFit) {
      this.gridColumnApi.autoSizeAllColumns();
      this.isColFit = false ;
    } else {
      this.gridApi.sizeColumnsToFit();
      this.isColFit = true ;
    }
  }
   
  /**
   * onRowClicked: Click Event On GridRow
   *
   * Button Edit & Delete
   * 
   * @param any e
   * @returns void
   */
  onRowClicked(e) {
   // console.log('OnRowClicked', e);
    if (e.event.target !== undefined) {
      const data = e.data;
      const actionType = e.event.target.getAttribute('data-action-type');
      switch (actionType) {
          case 'edit':
              return this.onActionEditClick(data);
          case 'delete':
              return this.onActionDeleteClick(data);
      }
    }
  }
     
  /**
   * onKeyp: Trigger this Func While typing
   * Filter Grid onKeyp
   * 
   * @param any event
   * @returns void
   */
  onKeyp(event) {
    this.gridApi.setQuickFilter(event.target.value);
  }

  /**
   * onActionEditClick: Click Edit button Grid
   * 
   * @param any data
   * @returns void
   */
  onActionEditClick(data: any) {
    // Check if Permissions Exist
    const isHasPermission = this.permissionsService.hasPermission('display_propriete');
    isHasPermission.then(res=> { 
      if(!res) return ; // If display_propriete Disabled return
      this.router.navigate(['/home/propriete/edit/' + data.id]); // If Enabled
     });
  }
  
  /**
   * onActionDeleteClick: Click Delete button Grid
   * 
   * @param any data
   * @returns void
   */
  onActionDeleteClick(data: any) {
    // Check if Permissions Exist
    const isHasPermission = this.permissionsService.hasPermission('delete_proprietaire');
    isHasPermission.then(res=> { 
      if(!res){ // If delete_proprietaire Disabled return
        this.snotifyService.warning('Intérdit, Contacter l\'admin', this.globalObject.configNotify);
        return ;
      }
      // Else
        this.snotifyService.confirm('Êtes-vous sûr de vouloir supprimer ce propriete ?', {
          timeout: 8000,
          position: 'centerTop',
          buttons: [
            {text: 'Oui', action: () => this.deleteRow(data.id), bold: false},
            {text: 'Non', action: (toast) => this.snotifyService.remove(toast.id)},
            {text: 'Fermer', action: (toast) => {this.snotifyService.remove(toast.id); }, bold: true},
          ]
        });
        
    });
  }
  
  /**
   * Delete Row
   * 
   * @param int id (ids of rows)
   * @returns void
   */
  deleteRow(id) {
    this.snotifyService.clear();
    this.spinnerService.show();
    this.proprieteService.delete(id).subscribe(res => {
             this.spinnerService.hide();
    });
  }
  
  /**
   * OnDeleteSeletedRows: Click Event On Button Delete Selected
   * 
   * Button Selected Rows
   * 
   * @param any data
   * @returns void
   */
  onDeleteSeletedRows() {
   this.snotifyService.confirm('Êtes-vous sûr de vouloir supprimer ces proprietes ?', {
      timeout: 8000,
      position: 'centerTop',
      buttons: [
        {text: 'Oui', action: () => this.deleteSelectedRows(), bold: false},
        {text: 'Non', action: (toast) => this.snotifyService.remove(toast.id)},
        {text: 'Fermer', action: (toast) => {this.snotifyService.remove(toast.id); }, bold: true},
      ]
    });
  }

  /**
   * deleteSelectedRows
   * 
   * Delete Selected Rows From Grid
   * 
   * @returns void
   */
  deleteSelectedRows () {
    const ids = new Array() ;
    this.gridApi.getSelectedRows().forEach(element => {
      ids.push(element.id) ;
    });

    this.snotifyService.clear();
    this.spinnerService.show();
    this.proprieteService.delete(ids).subscribe(res => {
      this.isRowsSelected = false ;
      this.spinnerService.hide();
    });
  }

  /**
   * synchronize
   * 
   * Sync Data after Any opertation (insert/update/delete)
   * 
   * @returns void
   */
  synchronize() {
    this.msgSynchronizeSubscription = this.globaleService.get_Observable_MsgSynchronize().subscribe(res => {
          if (!res) {
            return ;
          }
          if (!res.message) {
            return ;
          }
          if (!res.message.original) {
            return ;
          }
          if (!res.message.original.propriete) {
            return ;
          }
          const message = res.message.original.propriete ;

          // Insert
          if (message.insertedObject) {
              if (message.insertedObject.fk_copropriete === this.coproprieteExercice.copropriete_id) {
                  // Insert to Grid
                  this.gridClass.insertToGrid(this.gridApi, message.insertedObject, this.model);
                }
            }

          // Update
          if (message.update) {
            if (message.updatedObject.fk_copropriete === this.coproprieteExercice.copropriete_id) {
              // Update the Grid
              this.gridClass.updateGrid(this.gridApi, message.update, this.model);
            }
          }

          // Delete
          if (message.deleted_id) {
            if (message.copropriete_id === this.coproprieteExercice.copropriete_id) {
              // Delete From Grid
              this.gridClass.deleteFromGrid(this.gridApi, message.deleted_id, this.model);
            }
          }

          // Restore
          if (message.restoredObjects) {
            if (message.copropriete_id === this.coproprieteExercice.copropriete_id) {
                // Restore to Grid
                this.gridClass.restoreGrid(
                                           this.gridApi, 
                                           message.restoredObjects, 
                                           message.restored_id, 
                                           this.model
                                          );
              }
          }

          // clear msg
          if (res.message) {
             this.globaleService.setMsgSynchronize(null) ;
          }
      });
  }

}
