import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeProprieteComponent } from './liste-propriete.component';

describe('ListeProprieteComponent', () => {
  let component: ListeProprieteComponent;
  let fixture: ComponentFixture<ListeProprieteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeProprieteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeProprieteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
