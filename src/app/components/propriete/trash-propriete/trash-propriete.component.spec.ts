import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrashProprieteComponent } from './trash-propriete.component';

describe('TrashProprieteComponent', () => {
  let component: TrashProprieteComponent;
  let fixture: ComponentFixture<TrashProprieteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrashProprieteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrashProprieteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
