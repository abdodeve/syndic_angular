import { Injectable, OnInit, OnDestroy } from '@angular/core';
import { GlobaleService, GlobalObject } from '../../../services/globale.service';
import { Observable } from 'rxjs' ;
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Propriete } from '../propriete';

@Injectable({
  providedIn: 'root'
})
export class ProprieteService implements OnInit, OnDestroy {


  /*
  * Syncronous variable
  */
  private globalObject: GlobalObject = new GlobalObject() ;
  private coproprieteExercice ;
  private globalObjectSubscription ;


  constructor(private http: HttpClient, private globaleService: GlobaleService) {
        // Subscribe to globaleService
        this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject()
        .subscribe(globalObject => this.globalObject = globalObject) ;
   }


  ngOnInit() {

  }

  ngOnDestroy () {
    this.globalObjectSubscription.unsubscribe();
  }

  /**
   * Get Propriete
   * 
   * @param int id_copropriete 
   * @returns array proprietaires
   */
  fetch( id_copropriete ): Observable <any> {

    const url             = this.globalObject.api_url + '/api/propriete/fetch' ;
    const body            = JSON.stringify({id_copropriete: id_copropriete});
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    return this.http.post<Propriete>(url, body, httpOptions) ;
  }

  /**
   * Get Single Propriete
   * 
   * @param int id
   * @returns object propriete
   */
  single( id ): Observable <any> {
    const url             = this.globalObject.api_url + '/api/propriete/single/' + id ;
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    return this.http.get<any>(url, httpOptions) ;
  }

  /**
   * Insert Propriete
   * 
   * @param object propriete
   * @returns object propriete
   */
  insert( propriete: Propriete ): Observable <any> {

    const url             = this.globalObject.api_url + '/api/propriete/insert' ;
    const body            = JSON.stringify(propriete);
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token')),
                          })
                        };
    return this.http.post<Propriete>(url, body, httpOptions) ;
  }

  /**
   * Update Propriete
   * 
   * @param int id
   * @param object propriete
   * 
   * @returns object propriete
   */
  update( id, propriete: Propriete ): Observable <any> {

    const url             = this.globalObject.api_url + '/api/propriete/update/' + id ;
    const body            = JSON.stringify(propriete);
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    return this.http.put<Propriete>(url, body, httpOptions) ;
  }

  /**
   * Delete Propriete
   * 
   * @param int id
   * 
   * @returns object propriete
   */
  delete( id ): Observable <any> {

    const url             = this.globalObject.api_url + '/api/propriete/delete' ;
    const body            = JSON.stringify( {'id': id} );
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    return this.http.post<any>(url, body, httpOptions) ;
  }

 /*************************************************************************
  * Restoring side
  *************************************************************************/

  /**
   * Get Deleted Proprietaires
   * 
   * @param int id_copropriete
   * 
   * @returns array proprietaires
   */
  fetchDeleted( id_copropriete ): Observable <any> {

    const url             = this.globalObject.api_url + '/api/propriete/fetchDeleted' ;
    const body            = JSON.stringify({id_copropriete: id_copropriete});
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    return this.http.post<Propriete>(url, body, httpOptions) ;
  }

  /**
   * Restore Propriete
   * 
   * @param int id
   * 
   * @returns object propriete
   */
  restore( id ): Observable <Propriete> {

    const url             = this.globalObject.api_url + '/api/propriete/restore' ;
    const body            = JSON.stringify({'id': id});
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    return this.http.post<Propriete>(url, body, httpOptions) ;
  }

  /**
   * Delete Propriete Permanently
   * 
   * @param int id
   * 
   * @returns object propriete
   */
  deletePermanently( id ): Observable <any> {

    const url             = this.globalObject.api_url + '/api/propriete/deletePermanently' ;
    const body            = JSON.stringify( {'id': id} );
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    return this.http.post<any>(url, body, httpOptions) ;
  }

}
