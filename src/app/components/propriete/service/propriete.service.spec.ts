import { TestBed, inject } from '@angular/core/testing';

import { ProprieteService } from './propriete.service';

describe('ProprieteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProprieteService]
    });
  });

  it('should be created', inject([ProprieteService], (service: ProprieteService) => {
    expect(service).toBeTruthy();
  }));
});
