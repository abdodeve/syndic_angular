import { TestBed, inject } from '@angular/core/testing';

import { ProprietaireServiceService } from './proprietaire-service.service';

describe('ProprietaireServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProprietaireServiceService]
    });
  });

  it('should be created', inject([ProprietaireServiceService], (service: ProprietaireServiceService) => {
    expect(service).toBeTruthy();
  }));
});
