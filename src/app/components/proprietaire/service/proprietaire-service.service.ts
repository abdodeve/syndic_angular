import { Injectable, OnInit, OnDestroy } from '@angular/core';
import { GlobaleService, GlobalObject } from '../../../services/globale.service';
import { Observable } from 'rxjs' ;
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Proprietaire } from '../proprietaire';


@Injectable()
export class ProprietaireServiceService implements OnInit, OnDestroy {


  /*
  * Syncronous variable
  */
  private globalObject: GlobalObject = new GlobalObject() ;
  private coproprieteExercice ;
  private globalObjectSubscription ;


  constructor(private http: HttpClient, private globaleService: GlobaleService) {
        // Subscribe to globaleService
        this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject()
                                                           .subscribe(globalObject => this.globalObject = globalObject) ;
   }


  ngOnInit() {
  }

  ngOnDestroy () {
    this.globalObjectSubscription.unsubscribe();
  }

  /**
   * Get Proprietaire
   * 
   * @param int id_copropriete 
   * @returns array proprietaires
   */
  fetch( id_copropriete ): Observable <any> {

    const url             = this.globalObject.api_url + '/api/proprietaire/fetch' ;
    const body            = JSON.stringify({id_copropriete: id_copropriete});
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    return this.http.post<Proprietaire>(url, body, httpOptions) ;
  }

  /**
   * Get Single Propriete
   * 
   * @param int id
   * @returns object proprietaire
   */
  single( id ): Observable <any> {
    const url             = this.globalObject.api_url + '/api/proprietaire/single/' + id ;
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    return this.http.get<any>(url, httpOptions) ;
  }

  /**
   * Insert Proprietaire
   * 
   * @param object proprietaire
   * @returns object proprietaire
   */
  insert( proprietaire: Proprietaire ): Observable <any> {

    const url             = this.globalObject.api_url + '/api/proprietaire/insert' ;
    const body            = JSON.stringify(proprietaire);
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token')),
                          })
                        };
    return this.http.post<Proprietaire>(url, body, httpOptions) ;
  }

  /**
   * Update Proprietaire
   * 
   * @param int id
   * @param object proprietaire
   * 
   * @returns object proprietaire
   */
  update( id, proprietaire: Proprietaire ): Observable <any> {

    const url             = this.globalObject.api_url + '/api/proprietaire/update/' + id ;
    const body            = JSON.stringify(proprietaire);
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    return this.http.put<Proprietaire>(url, body, httpOptions) ;
  }

  /**
   * Delete Proprietaire
   * 
   * @param int id
   * 
   * @returns object proprietaire
   */
  delete( id ): Observable <any> {

    const url             = this.globalObject.api_url + '/api/proprietaire/delete' ;
    const body            = JSON.stringify( {'id': id} );
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    return this.http.post<any>(url, body, httpOptions) ;
  }

  /************************************************************************
   * Restoring side
   ************************************************************************/

  /**
   * Get Deleted Proprietaires
   * 
   * @param int id_copropriete
   * 
   * @returns array proprietaires
   */
  fetchDeleted( id_copropriete ): Observable <any> {

    const url             = this.globalObject.api_url + '/api/proprietaire/fetchDeleted' ;
    const body            = JSON.stringify({id_copropriete: id_copropriete});
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    return this.http.post<Proprietaire>(url, body, httpOptions) ;
  }

  /**
   * Restore Proprietaire
   * 
   * @param int id
   * 
   * @returns object proprietaire
   */
  restore( id ): Observable <Proprietaire> {

    const url             = this.globalObject.api_url + '/api/proprietaire/restore' ;
    const body            = JSON.stringify({'id': id});
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    return this.http.post<Proprietaire>(url, body, httpOptions) ;
  }

  /**
   * Delete Proprietaire Permanently
   * 
   * @param int id
   * 
   * @returns object proprietaire
   */
  deletePermanently( id ): Observable <any> {

    const url             = this.globalObject.api_url + '/api/proprietaire/deletePermanently' ;
    const body            = JSON.stringify( {'id': id} );
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    return this.http.post<any>(url, body, httpOptions) ;
  }

  /**
   * Exporter Proprietaire
   * 
   * @returns string message
   */
  exporter(): Observable <Blob> {

    const url             = this.globalObject.api_url + '/api/proprietaire/export' ;
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          }),
                          responseType: 'blob' as 'json'
                        };
    return this.http.get<Blob>(url, httpOptions) ;
  }


  /**
   * Importer Proprietaire
   * 
   * @param copropriete_id string
   * @param formData FormData
   * 
   * @returns string message
   */
  importer(copropriete_id: string, formData: FormData): Observable <any> {
    const url             = this.globalObject.api_url + '/api/proprietaire/import' ;
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token')),
                            'copropriete_id': copropriete_id.toString()
                          })
                        };
    return this.http.post<any>(url, formData, httpOptions) ;
  }

  /**
   * Generate Proprietaire PDF
   * 
   * @param copropriete_id string
   * @param formData FormData
   * 
   * @returns string message
   */
  generatePDF(): Observable <Blob> {

    const url         = this.globalObject.api_url + '/api/proprietaire/generatePDF' ;
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          }),
                          responseType: 'blob' as 'json'
                        };
    return this.http.get<Blob>(url, httpOptions) ;
  }

}
