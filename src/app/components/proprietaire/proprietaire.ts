export class Proprietaire {
    id? ;
    fk_copropriete? ;
    nom?: string ;
    prenom?: string ;
    cin? ;
    profession? ;
    titre? ;
    ville? ;
    adresse_1? ;
    adresse_2? ;
    email_1? ;
    email_2? ;
    tel_1? ;
    tel_2? ;
    code_postale? ;
    description? ;
}
