import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm, Form } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { GlobaleService, GlobalObject } from '../../../services/globale.service';
import { Proprietaire } from '../proprietaire' ;
import { ProprietaireServiceService } from '../service/proprietaire-service.service';

@Component({
  selector: 'app-maj-proprietaire',
  templateUrl: './maj-proprietaire.component.html',
  styleUrls: ['./maj-proprietaire.component.css']
})
export class MajProprietaireComponent implements OnInit, OnDestroy {

  /*
  * Globale variables
  */
 private globalObject: GlobalObject = new GlobalObject() ;
 private proprietaire: Proprietaire = new Proprietaire() ;
 private id ;
 private isNew = true ;
 private next ;
 private previous ;
 private globalObjectSubscription ;
 private copropriete ;
 private paramsFromParent = null ;

 constructor(private globaleService: GlobaleService,
             private proprietaireService: ProprietaireServiceService,
             private snotifyService: SnotifyService,
             private activatedroute: ActivatedRoute,
             private router: Router,
             private spinnerService: Ng4LoadingSpinnerService,
           ) {

    // Subscribe to globaleService (globalObject)
    this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject()
                       .subscribe(globalObject => this.globalObject = globalObject) ;
    this.globaleService.get_Observable_CoproprieteExercice().subscribe(res => {
        if (!res) {
          return ;
        }
        this.copropriete = res ;
    });
  }

  ngOnInit(): void {
    this.NewOrEdit();
    this.setGlobalObject() ;
  }

  ngOnDestroy () {
    this.globalObjectSubscription.unsubscribe();
  }

  /**
   * Set Default GlobalObject
   *  
   * @returns void
   */
  setGlobalObject(): void {
    let globalObjectNew: GlobalObject ;
    // Check if you are on New or Edit
    if (this.isNew) {
          globalObjectNew = {
                              'title': 'Nouveau proprietaire',
                              'breadcrumb': ['Proprietaire', 'Nouveau']
                            } ;
    } else {
          globalObjectNew = {
                              'title': 'Editer proprietaire',
                              'breadcrumb': ['Proprietaire', 'Editer']
                            } ;
    }
    const mergeObjects = {...this.globalObject, ...globalObjectNew} ;
    this.globaleService.setGlobalObject(mergeObjects);
  }

  /**
   * onValidate: Click Event On Validate Btn
   * 
   * On Validate Click - insert/update
   * 
   * @param NgForm form
   * @returns void
   */
  onValidate(form: NgForm) {

    this.spinnerService.show();
    if (form.invalid) {
      return;
    }
    if ( this.isNew ) {
      this.proprietaire.fk_copropriete = this.copropriete.copropriete_id ;
      this.proprietaireService.insert( this.proprietaire ).subscribe( data => {
        this.spinnerService.hide();
        this.router.navigate(['/home/proprietaire/liste']);
      });
    } else {
      this.proprietaireService.update( this.id, this.proprietaire ).subscribe( data => {
        this.spinnerService.hide();
        this.router.navigate(['/home/proprietaire/liste']);
      });
    }
  }

  /**
   * onDelete: Click event on delete Btn
   * 
   * On Delete Click
   * 
   * @returns void
   */
  onDelete() {
    this.snotifyService.confirm('Êtes-vous sûr de vouloir supprimer ce proprietaire ?', {
      timeout: 8000,
      position: 'centerTop',
      buttons: [
        {text: 'Oui', action: () => this.deleteConfirmed(this.id), bold: false},
        {text: 'Non', action: (toast) => this.snotifyService.remove(toast.id)},
        {text: 'Fermer', action: (toast) => {this.snotifyService.remove(toast.id); }, bold: true},
      ]
    });
  }

  /**
   * onCancel: Click Event on Cancel Btn
   * 
   * On Cancel Click
   * 
   * @returns void
   */
  onCancel() {
    this.router.navigate(['/home/proprietaire/liste']);
  }


  /**
   * onNextStep: Click Event on NextStep Btn
   * 
   * On NextStep Click
   * 
   * @returns void
   */
  onNextStep(form: NgForm) {

    // this.router.navigate(['/home/proprietaire/edit/120']);
    // let btnTabAffectation = document.getElementById('nav-btn-proprietes-affectes');
    // btnTabAffectation.click();
 
    this.spinnerService.show();
    if (form.invalid) {
      return;
    }

    if ( this.isNew ) {
      this.proprietaire.fk_copropriete = this.copropriete.copropriete_id ;
      this.proprietaireService.insert( this.proprietaire ).subscribe( data => {
        this.spinnerService.hide();
        this.router.navigate(['/home/proprietaire/edit/'+data.inserted_proprietaire.id]).then( res => {
              // Trigger [Next TAB] click  | Go to proprietes-affectes
              document.getElementById('nav-btn-proprietes-affectes').click();
            } 
          );
      });
    }
  }

  
  /**
   * Get Single Proprietaire
   * 
   * @returns void
   */
  single() {
    this.spinnerService.show();
    this.proprietaireService.single(this.id).subscribe( res => {
      this.spinnerService.hide();
      // If id not found
      if(res.error == "not_found")   {
        this.snotifyService.warning('Proprietaire introuvable !', this.globalObject.configNotify);
        this.router.navigate(['/home/proprietaire/liste']);
        return ;
      }

      // Is Proprietaire Exist
      if(res.proprietaire) {
        this.proprietaire = res.proprietaire ;
        this.next = res.next ;
        this.previous = res.previous ;
        this.paramsFromParent = { id: this.id } ;
      } 
      // Is not Exist
      else {
        this.router.navigate(['/home']);
      }
 
      } 
    );
  }

  /**
   * Check mode - If New or Edit
   * 
   * @returns void
   */
  NewOrEdit() {
    this.activatedroute.params.subscribe(params => {
      this.id = params['id'] ;
        if (!this.id) 
          return ;
          this.paramsFromParent = { id: this.id } ;
            this.isNew = false ;
            this.single();
    });
  }

  /**
   * Delete confirmed
   * 
   * @param int id
   * @returns void
   */
  deleteConfirmed(id) {
    this.snotifyService.clear();
    this.spinnerService.show();
    this.proprietaireService.delete(id).subscribe(res => {
          this.spinnerService.hide();
          this.router.navigate(['/home/proprietaire/liste']);
     });
  }

  /**
   * Check If is the End of proprietaire (previous/next)
   * 
   * @param String nextOrPrevious
   * @returns void
   */
  isEnd(nextOrPrevious) {
    if (nextOrPrevious === 'next') {
      return this.next == null ? true : false ;
    }
    if (nextOrPrevious === 'previous') {
      return this.previous == null ? true : false ;
    }
  }

}
