import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MajProprietaireComponent } from './maj-proprietaire.component';

describe('MajProprietaireComponent', () => {
  let component: MajProprietaireComponent;
  let fixture: ComponentFixture<MajProprietaireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MajProprietaireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MajProprietaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
