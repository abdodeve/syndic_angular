import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Router, ActivatedRoute} from '@angular/router';
import { AgGridNg2 } from 'ag-grid-angular';
import { GridApi, ColumnApi, Events } from 'ag-grid';
import {SnotifyService, SnotifyPosition, SnotifyToastConfig} from 'ng-snotify';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { GlobaleService, GlobalObject } from '../../../services/globale.service';
import { Proprietaire } from '../proprietaire' ;
import { ProprietaireServiceService } from '../service/proprietaire-service.service';
import { GridClass } from '../../../services/grid.class' ;

@Component({
  selector: 'app-trash-proprietaire',
  templateUrl: './trash-proprietaire.component.html',
  styleUrls: ['./trash-proprietaire.component.css']
})
export class TrashProprietaireComponent implements OnInit, OnDestroy {

  /*
  * Globale variables
  */
 private globalObject: GlobalObject = new GlobalObject() ;
 private columnDefs = [
   { headerName: 'Nom', field: 'nom',
     headerCheckboxSelection: true,
     headerCheckboxSelectionFilteredOnly: false,
     checkboxSelection: true
   },
   {headerName: 'Prenom', field: 'prenom' },
   {headerName: 'Titre', field: 'titre'},
   {
     headerName: 'Action',
     suppressMenu: true,
     suppressSorting: true,
     width: 130,
     template: `
               <button type="button" data-action-type="restore"
                 class="btn btn-icon waves-effect waves-light btn-info" title="Réstaurer">
                 <i class="fa fa-history" data-action-type="restore"></i>
               </button>

               <button type="button" data-action-type="delete"
                 class="btn btn-icon waves-effect waves-light btn-danger" title="Supprimer">
                 <i class="fa fa-trash" data-action-type="delete"></i>
               </button>
               `
   }
 ];
 private rowData: any ;
 private gridApi: GridApi ;
 private gridColumnApi: ColumnApi ;
 private isColFit = true ;
 private isRowsSelected = false ;
 private getRowNodeId ;
 private msgSynchronizeSubscription ;
 private globalObjectSubscription ;
 private coproprieteExercice ;
 private coproprieteExerciceSubscription ;

constructor(private globaleService: GlobaleService,
           private http: HttpClient,
           private proprietaireService: ProprietaireServiceService,
           private snotifyService: SnotifyService,
           private activatedroute: ActivatedRoute,
           private router: Router,
           private spinnerService: Ng4LoadingSpinnerService,
           private gridClass: GridClass) {
   // Subscribe to globaleService (globalObject)
   this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject()
                      .subscribe(globalObject => this.globalObject = globalObject) ;
   // Subscribe to coproprieteExercice
   this.coproprieteExerciceSubscription = this.globaleService.get_Observable_CoproprieteExercice()
         .subscribe( res => {
                                if (!res) { return ; }
                                this.coproprieteExercice = res ;
                                this.setDataToGrid(this.coproprieteExercice.copropriete_id);
                             }
                   );
   this.setGlobalObject() ;
   this.getRowNodeId = function(data) {
     if (data) {
       return data.id;
     }
   };
   this.synchronize();
}

 ngOnInit() {
 }

 ngOnDestroy () {
   this.globalObjectSubscription.unsubscribe();
   this.coproprieteExerciceSubscription.unsubscribe();
   this.msgSynchronizeSubscription.unsubscribe();
 }

  /**
   * Set data to Grid
   * 
   * @param int id_copropriete
   * @returns void
   */
 setDataToGrid( id_copropriete = null ) {
   this.spinnerService.show();
   this.proprietaireService.fetchDeleted(id_copropriete).subscribe(res => {
     this.rowData = res.fetch_deleted_proprietaires ;
     this.spinnerService.hide();
   });
 }
 
  /**
   * On grid ready
   * Init this.gridApi
   * Init this.gridColumnApi
   * 
   * @param any params
   * @returns void
   */
 onGridReady(params) {
   // Assign grid api to local vars
   this.gridApi = params.api;
   this.gridColumnApi = params.columnApi;
   // Fit columns for Fill screen
   this.gridApi.sizeColumnsToFit();
 }

  /**
   * Set Default GlobalObject
   *  
   * @returns void
   */
 setGlobalObject(): void {
   const globalObjectNew: GlobalObject = {
                                          'title': 'Corbeille proprietaire',
                                          'breadcrumb': ['Proprietaire', 'Corbeille']
                                         } ;
   const mergeObjects = {...this.globalObject, ...globalObjectNew} ;
   this.globaleService.setGlobalObject(mergeObjects);
 }

  /**
   * onRowSelected: Click Event Select Grid
   * 
   * If more than 1 row selected
   * 
   * Set this.isRowsSelected to true
   * 
   * For Display btn 'Supprimer sélections'
   * 
   * @param any params
   * @returns void
   */
 onRowSelected(event) {
   const selectedRows = this.gridApi.getSelectedRows();
   if (selectedRows.length > 1) {
     this.isRowsSelected = true ;
   } else {
     this.isRowsSelected = false ;
   }
 }
  
  /**
   * onDoubleRowClicked: Double Click Event On GridRow
   * 
   * Double Click Grid Event
   * 
   * autoSize/fitSize Grid
   * 
   * @param any event
   * @returns void
   */
   onDoubleRowClicked(event) {
   if (this.isColFit) {
     this.gridColumnApi.autoSizeAllColumns();
     this.isColFit = false ;
   } else {
     this.gridApi.sizeColumnsToFit();
     this.isColFit = true ;
   }
 }
   
  /**
   * onRowClicked: Click Event On GridRow
   *
   * Button Edit & Delete
   * 
   * @param any e
   * @returns void
   */
 onRowClicked(e) {
  // console.log('OnRowClicked', e.event.target);
   if (e.event.target !== undefined) {
     const data = e.data;
     const actionType = e.event.target.getAttribute('data-action-type');
     switch (actionType) {
         case 'restore':
             return this.onActionRestoreClick(data);
         case 'delete':
             return this.onActionDeleteClick(data);
     }
   }
 }    
     
  /**
   * onKeyp: Trigger this Func While typing
   * Filter Grid onKeyp
   * 
   * @param any event
   * @returns void
   */
 onKeyp(event) {
   this.gridApi.setQuickFilter(event.target.value);
 }

 /**
   * onActionRestoreClick: Click On Restore button (inside Grid)
   * 
   * Action Restore
   * 
   * @param any data
   * @returns void
   */
 onActionRestoreClick(data: any) {
  this.spinnerService.show();
  this.proprietaireService.restore(data.id).subscribe( res => {
    this.spinnerService.hide();
  });
 }

/**
 * onActionDeleteClick: Click On Delete button (inside Grid)
 * 
 * Action Delete
 * 
 * @param any data
 * @returns void
 */
 onActionDeleteClick(data: any) {
   this.snotifyService.confirm('Êtes-vous sûr de vouloir supprimer ce proprietaire définitivement ?', {
     timeout: 8000,
     position: 'centerTop',
     buttons: [
       {text: 'Oui', action: () => this.deleteRow(data.id), bold: false},
       {text: 'Non', action: (toast) => this.snotifyService.remove(toast.id)},
       {text: 'Fermer', action: (toast) => {this.snotifyService.remove(toast.id); }, bold: true},
     ]
   });
 }
  
  /**
   * Delete Row
   * 
   * @param int id (ids of rows)
   * @returns void
   */
 deleteRow(id) {
   this.snotifyService.clear();
   this.spinnerService.show();
   this.proprietaireService.deletePermanently(id).subscribe(res => {
            this.spinnerService.hide();
   });
 }
  
  /**
   * OnDeleteSeletedRows: Click Event On Delete Selected
   * 
   * Button Selected Rows From delete multiple rows
   * 
   * @param any data
   * @returns void
   */
 onDeleteSeletedRows() {
  this.snotifyService.confirm('Êtes-vous sûr de vouloir supprimer ces proprietaires ?', {
     timeout: 8000,
     position: 'centerTop',
     buttons: [
       {text: 'Oui', action: () => this.deleteSelectedRows(), bold: false},
       {text: 'Non', action: (toast) => this.snotifyService.remove(toast.id)},
       {text: 'Fermer', action: (toast) => {this.snotifyService.remove(toast.id); }, bold: true},
     ]
   });
 }

  /**
   * deleteSelectedRows
   * 
   * Delete Selected Rows From Grid
   * 
   * @returns void
   */
  deleteSelectedRows () {
   const ids = new Array() ;
   this.gridApi.getSelectedRows().forEach(element => {
     ids.push(element.id) ;
   });

   this.snotifyService.clear();
   this.spinnerService.show();
   this.proprietaireService.deletePermanently(ids).subscribe(res => {
     this.spinnerService.hide();
     this.isRowsSelected = false ;
   });
 }

/**
 * onRestoreSeletedRows: Click Event on Restore Btn
 * 
 * Button Restore
 * 
 * @param any data
 * @returns void
 */
 onRestoreSeletedRows() {
  this.snotifyService.confirm('Êtes-vous sûr de vouloir réstaurer ces proprietaires ?', {
     timeout: 8000,
     position: 'centerTop',
     buttons: [
       {text: 'Oui', action: () => this.restoreRows(), bold: false},
       {text: 'Non', action: (toast) => this.snotifyService.remove(toast.id)},
       {text: 'Fermer', action: (toast) => {this.snotifyService.remove(toast.id); }, bold: true},
     ]
   });
 }

/**
 * restoreRows: Restore selected rows
 *
 * @returns void
 */
 restoreRows () {
  const ids = new Array() ;
  this.gridApi.getSelectedRows().forEach(element => {
    ids.push(element.id) ;
  });

  this.snotifyService.clear();
  this.spinnerService.show();
  this.proprietaireService.restore(ids).subscribe(res => {
    this.spinnerService.hide();
    this.isRowsSelected = false ;
  });
}


/**
 * synchronize
 * 
 * Sync Data after Any opertation (insert/update/delete)
 * 
 * @returns void
 */
 synchronize() {
   this.msgSynchronizeSubscription = this.globaleService.get_Observable_MsgSynchronize().subscribe(res => {
         if (!res) {
           return ;
         }
         if (!res.message) {
           return ;
         }
         if (!res.message.original) {
           return ;
         }
         if (!res.message.original.proprietaire) {
           return ;
         }
         const message = res.message.original.proprietaire ;

         // Restore
         if (message.restored_id) {
            if (message.copropriete_id === this.coproprieteExercice.copropriete_id) {
              this.gridClass.deleteMultiple(this.gridApi, message.restored_id);

              let msg = `Proprietaire: ${message.restored_id} est réstauré` ;
              if (message.restored_id.length > 1) {
                msg = `Proprietaires: ${message.restored_id} sont réstauré` ;
              }
              this.snotifyService.info(msg, this.globalObject.configNotify);
           }
         }
        // Delete Permanently
        if (message.deletedPermanently_id) {
          if (message.copropriete_id === this.coproprieteExercice.copropriete_id) {
            this.gridClass.deleteMultiple(this.gridApi, message.deletedPermanently_id);

            let msg = `Proprietaire: ${message.deletedPermanently_id} est supprimé définitivement` ;
            if (message.deletedPermanently_id.length > 1) {
              msg = `Proprietaires: ${message.deletedPermanently_id} sont supprimé définitivement` ;
            }
            this.snotifyService.info(msg, this.globalObject.configNotify);
            }
          }
          // Add to trash
          if (message.deletedObjects) {
            if (message.copropriete_id === this.coproprieteExercice.copropriete_id) {
              this.gridClass.insert(this.gridApi, message.deletedObjects);

              let msg = `Proprietaire: ${message.deleted_id} est déplacé à la corbeille` ;
              if (message.deleted_id.length > 1) {
                msg = `Proprietaires: ${message.deleted_id} sont déplacé à la corbeille` ;
              }
              this.snotifyService.info(msg, this.globalObject.configNotify);
              }
          }

         // clear msg
         if (res.message) {
            this.globaleService.setMsgSynchronize(null) ;
         }
     });
 }

}
