import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrashProprietaireComponent } from './trash-proprietaire.component';

describe('TrashProprietaireComponent', () => {
  let component: TrashProprietaireComponent;
  let fixture: ComponentFixture<TrashProprietaireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrashProprietaireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrashProprietaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
