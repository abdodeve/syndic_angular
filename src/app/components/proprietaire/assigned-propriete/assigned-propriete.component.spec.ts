import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignedProprieteComponent } from './assigned-propriete.component';

describe('AssignedProprieteComponent', () => {
  let component: AssignedProprieteComponent;
  let fixture: ComponentFixture<AssignedProprieteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignedProprieteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignedProprieteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
