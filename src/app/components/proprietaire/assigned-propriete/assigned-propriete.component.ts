import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GridApi, ColumnApi, Events } from 'ag-grid';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { GlobaleService, GlobalObject } from '../../../services/globale.service';
import { GridClass } from '../../../services/grid.class' ;
import { NgxPermissionsService } from 'ngx-permissions' ;
import { AssignedProprieteService } from './service/assigned-propriete.service' ;
import { ProprieteService } from '../../propriete/service/propriete.service' ;

@Component({
  selector: 'app-assigned-propriete',
  templateUrl: './assigned-propriete.component.html',
  styleUrls: ['./assigned-propriete.component.css']
})
export class AssignedProprieteComponent implements OnInit, OnDestroy {

  @Input() parentParams ;
  private proprietaire_id ;
  
  /*
  * Globale variables
  */
 private globalObject: GlobalObject = new GlobalObject() ;
 private columnDefs = [
   {headerName: 'Num propriete', field: 'num_propriete', checkboxSelection: true },
   {headerName: 'Type copropriete', field: 'type_copropriete' },
   {headerName: 'Etage', field: 'etage'},
   {headerName: 'Num titre', field: 'num_titre'},
   {headerName: 'Surface', field: 'surface'},
   {
     headerName: 'Action',
     suppressMenu: true,
     suppressSorting: true,
     width: 130,
     template: `
               <button type="button" data-action-type="desaffecter"
                 class="btn btn-icon waves-effect waves-light btn-danger" title="Désaffecter">
                 <i class="fa fa-unlink" data-action-type="desaffecter"></i>
               </button>
               `
   }
 ];
  // Grid vars
  private rowData: any ;
  private gridApi: GridApi ;
  private gridColumnApi: ColumnApi ;
  private isColFit = true ;
  private isRowsSelected_assignedList = false ;
  private isRowsSelected_unassignedList = false ;
  private getRowNodeId ;

  // Other vars
  private msgSynchronizeSubscription ;
  private globalObjectSubscription ;
  private coproprieteExercice ;
  private coproprieteExerciceSubscription ;
  private isAssignedList = "assigned_list" ;

  constructor(private globaleService: GlobaleService,
              private snotifyService: SnotifyService,
              private spinnerService: Ng4LoadingSpinnerService,
              private gridClass: GridClass,
              private permissionsService: NgxPermissionsService,
              private assignedProprieteService: AssignedProprieteService,
              private proprieteService: ProprieteService,
              private activatedroute: ActivatedRoute,)  {

          // Subscribe to globaleService (globalObject)
          this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject()
                            .subscribe(globalObject => this.globalObject = globalObject) ;

          // Subscribe to coproprieteExercice
          this.coproprieteExerciceSubscription = this.globaleService.get_Observable_CoproprieteExercice()
                                                                    .subscribe( res => {
                                                                          if (!res) { return ; }
                                                                            this.coproprieteExercice = res ;
                                                                          }
                                                                      );                  
          // Set the ID of data as RowNodeID
          this.getRowNodeId = function(data)  {
                                                if (data) return data.id;
                                              };


    }

  ngOnInit() {
      // Get parentParams (Id proprietaire)
      this.proprietaire_id = this.parentParams ? this.parentParams.id : null ;
      this.setDataToGrid();
      this.NewOrEdit();
  }

  ngOnDestroy () {
    this.globalObjectSubscription.unsubscribe();
    this.coproprieteExerciceSubscription.unsubscribe();
  }

   /**
   * Set data to Grid
   * 
   * @param int id_copropriete
   * @returns void
   */
  setDataToGrid() {
     if(!this.proprietaire_id) return ;
     this.assignedProprieteService.getProprietes(this.proprietaire_id).subscribe(res => {
       this.rowData = res.proprietes ;
    });  
  }

    /**
   * On grid ready
   * Init this.gridApi
   * Init this.gridColumnApi
   * 
   * @param any params
   * @returns void
   */
  onGridReady(params) {
    // Assign grid api to local vars
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }
  
  /**
   * onRowSelected: Click Event Select Grid
   * 
   * If more than 1 row selected
   * 
   * Set this.isRowsSelected to true
   * 
   * For Display btn 'Supprimer sélections'
   * 
   * @param any params
   * @returns void
   */
  onRowSelected(event = null) {
    const selectedRows = this.gridApi.getSelectedRows();
    let isMultipleRowsSelected: boolean = (selectedRows.length > 1) ? true : false ;

      switch ( this.isAssignedList ) {
        case "assigned_list":
            this.isRowsSelected_unassignedList = isMultipleRowsSelected ;
            break ;
        case "unassigned_list":
            this.isRowsSelected_assignedList = isMultipleRowsSelected ;
            break ;
        default:
            // unknown isAssignedList
            break ;
      }

  }


   /**
   * onDoubleRowClicked: Double Click Event On GridRow
   * 
   * Double Click Grid Event
   * 
   * autoSize/fitSize Grid
   * 
   * @param any event
   * @returns void
   */
  onDoubleRowClicked(event) {
    if (this.isColFit) {
      this.gridColumnApi.autoSizeAllColumns();
      this.isColFit = false ;
    } else {
      this.gridApi.sizeColumnsToFit();
      this.isColFit = true ;
    }
  }

     
  /**
   * onRowClicked: Click Event On GridRow
   *
   * Button Edit & Delete
   * 
   * @param any e
   * @returns void
   */
  onRowClicked(e) {
     if (e.event.target !== undefined) {
       const data = e.data;
       const actionType = e.event.target.getAttribute('data-action-type');
       switch (actionType) {
          case 'desaffecter':
                 this.onActionDesaffecterClick(data);
                 break ;
          case 'affecter':
                this.onActionAffecterClick(data.id);
                break ;
       }
     }
   }


  /**
   * onDesaffecterSeletedRows: Click Event On Button Desaffecter Selected
   * 
   * Button Selected Rows
   * 
   * @param any data
   * @returns void
   */
  onDesaffecterSeletedRows() {
    this.snotifyService.confirm('Êtes-vous sûr de vouloir désaffecter ces propeietés ?', {
       timeout: 8000,
       position: 'centerTop',
       buttons: [
         {text: 'Oui', action: () => this.desaffecterSelectedRows(), bold: false},
         {text: 'Non', action: (toast) => this.snotifyService.remove(toast.id)},
         {text: 'Fermer', action: (toast) => {this.snotifyService.remove(toast.id); }, bold: true},
       ]
     });
   }

  /**
   * desaffecterSelectedRows - Call API
   * 
   * Desaffecter Selected Rows From Grid
   * 
   * @returns void
   */
  desaffecterSelectedRows () {
    let ids = new Array() ;
    let propriete_proprietaire_ids = [] ;

    // Get selected rows
    this.gridApi.getSelectedRows().forEach(element => {
      ids.push(element.id) ;
      propriete_proprietaire_ids.push(element.propriete_proprietaire_id);
    });

    this.desaffecter(ids, propriete_proprietaire_ids);
  }
     
  /**
   * onActionDesaffecterClick: Click Desaffecter button Grid
   * 
   * @param any data
   * @returns void
   */
  onActionDesaffecterClick(data: any) {
        this.snotifyService.confirm('Êtes-vous sûr de vouloir désaffecter ce proprieté ?', {
          timeout: 8000,
          position: 'centerTop',
          buttons: [
            {text: 'Oui', action: () => this.desaffecter([data.id], data.propriete_proprietaire_id), bold: false},
            {text: 'Non', action: (toast) => this.snotifyService.remove(toast.id)},
            {text: 'Fermer', action: (toast) => {this.snotifyService.remove(toast.id); }, bold: true},
          ]
        });
  }

  
  /**
   * Desaffecter Row - Call API
   * 
   * @param int propriete_proprietaire_id - ID de propriete_proprietaire
   * @param int id (ids of rows)
   * @returns void
   */
  desaffecter(id, propriete_proprietaire_id) {
    this.snotifyService.clear();
    this.spinnerService.show();

    this.assignedProprieteService.DeleteProprieteProprietaire(propriete_proprietaire_id).subscribe(res => {
      this.spinnerService.hide();
      // Delete From Grid
      this.gridClass.deleteMultiple(this.gridApi, id);
      this.snotifyService.info(`Proprieté: ${id} est désaffecté`, this.globalObject.configNotify);
    });
  }


  /**
   * onAffecterSeletedRows: Click Event On Button Affecter Selected
   * 
   * Button Affecter Selected Rows
   * 
   * @param any data
   * @returns void
   */
  onAffecterSeletedRows() {
    let ids = new Array() ;

    // Get selected rows
    this.gridApi.getSelectedRows().forEach(element => {
      ids.push(element.id) ;
    });

    // Assign rows
    this.onActionAffecterClick(ids);
  }

  /**
   * Affecter Row
   * 
   * @param int propriete_proprietaire_id - ID de propriete_proprietaire
   * @param int id (ids of rows)
   * @returns void
   */
  onActionAffecterClick(id) {
    this.assignedProprieteService
                                  .assignProprietesToProprietaire(  this.coproprieteExercice.copropriete_id, 
                                                                    id, 
                                                                    this.proprietaire_id
                                                                 )
                                                                .subscribe(res => {
      this.snotifyService.info(`Proprieté: ${id} est affecté`, this.globalObject.configNotify);
          
      // Switch to unAssigned list
      let goBackward = document.getElementById('goBackward') ;
      goBackward.click();
    });
  }


    /**
   * Check mode - If New or Edit
   * 
   * @returns void
   */
  NewOrEdit() {
    this.activatedroute.params.subscribe(params => {
      let id = params['id'] ;
      if (!id)  
        return ;
      this.proprietaire_id = id ;
      this.setDataToGrid();

    });
  }

  /**
   * onProprietesAffectationSwitch: Click Event On Button (Retour/Affecter proprietés) 
   * We use this event For Switch betwen Assigned/Unassigned List
   *  
   * @param any data
   * @returns void
   */
  onProprietesAffectationSwitch(event) {
    let is_assigned_list_current = event.target.getAttribute('data-is_assigned_list') ;
    if (event.target === undefined) 
        return ;

      switch (is_assigned_list_current) {
          /*
          * Assigned Mode (Default Mode)
          * Now i will switch to UnAssigned mode
          */
          case 'assigned_list':
                this.isAssignedList = "unassigned_list" ;

                this.columnDefs = [
                  {headerName: 'Num propriete', field: 'num_propriete', checkboxSelection: true },
                  {headerName: 'Type copropriete', field: 'type_copropriete' },
                  {headerName: 'Etage', field: 'etage'},
                  {headerName: 'Num titre', field: 'num_titre'},
                  {headerName: 'Surface', field: 'surface'},
                  {
                    headerName: 'Action',
                    suppressMenu: true,
                    suppressSorting: true,
                    width: 130,
                    template: `
                              <button type="button" data-action-type="affecter"
                                class="btn btn-icon waves-effect waves-light btn-danger" title="Affecter">
                                <i class="fa fa-link" data-action-type="affecter"></i>
                              </button>
                              `
                  }
                ];

                
                this.proprieteService.fetch(this.coproprieteExercice.copropriete_id).subscribe(res => {
                  this.rowData = res.proprietes ;
                });
                
                break ;
          /* 
          * UnAssigned Mode
          * Now i will switch to Assigned mode
          */
          case 'unassigned_list':
                this.isAssignedList = "assigned_list" ;

                this.columnDefs = [
                  {headerName: 'Num propriete', field: 'num_propriete', checkboxSelection: true },
                  {headerName: 'Type copropriete', field: 'type_copropriete' },
                  {headerName: 'Etage', field: 'etage'},
                  {headerName: 'Num titre', field: 'num_titre'},
                  {headerName: 'Surface', field: 'surface'},
                  {
                    headerName: 'Action',
                    suppressMenu: true,
                    suppressSorting: true,
                    width: 130,
                    template: `
                              <button type="button" data-action-type="desaffecter"
                                class="btn btn-icon waves-effect waves-light btn-danger" title="Désaffecter">
                                <i class="fa fa-unlink" data-action-type="desaffecter"></i>
                              </button>
                              `
                  }
                ];

                  // Set Data to Grid
                  this.setDataToGrid();

                break ;
          default:
                // isAssignedList not Find
                break ;
      }


      // Set SelectedRows to False
      this.initSelectedRows();
  }

  /**
   * Set isRowsSelected to false
   * @returns void
   */
  initSelectedRows(): void{
      this.isRowsSelected_assignedList = false ;
      this.isRowsSelected_unassignedList = false ;
  }

}
