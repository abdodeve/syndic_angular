import { TestBed, inject } from '@angular/core/testing';

import { AssignedProprieteService } from './assigned-propriete.service';

describe('AssignedProprieteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AssignedProprieteService]
    });
  });

  it('should be created', inject([AssignedProprieteService], (service: AssignedProprieteService) => {
    expect(service).toBeTruthy();
  }));
});
