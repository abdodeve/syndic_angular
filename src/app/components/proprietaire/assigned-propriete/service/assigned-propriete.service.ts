import { Injectable, OnInit, OnDestroy } from '@angular/core';
import { GlobaleService, GlobalObject } from '../../../../services/globale.service';
import { Observable } from 'rxjs' ;
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AssignedProprieteService implements OnInit, OnDestroy {

/*
* Syncronous variable
*/
private globalObject: GlobalObject = new GlobalObject() ;
private coproprieteExercice ;
private globalObjectSubscription ;
  
constructor(private http: HttpClient, private globaleService: GlobaleService) {
    // Subscribe to globaleService
    this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject()
                                                       .subscribe(globalObject => this.globalObject = globalObject) ;
}


ngOnInit() {
}

ngOnDestroy () {
this.globalObjectSubscription.unsubscribe();
}

  /**
   * Get Assigned Proprietes to a Proprietaire
   * 
   * @param mixed proprietaire_ids
   * @returns array proprietes
   */
  getProprietes( proprietaire_ids ): Observable <any> {

    const url             = this.globalObject.api_url + '/api/ProprieteProprietaire/getProprietes' ;
    const body            = JSON.stringify({proprietaire_ids: proprietaire_ids});
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    return this.http.post<any>(url, body, httpOptions) ;
  }

  /**
   * Get Assigned Proprietaires to a Propriete
   * 
   * @param mixed proprietes_ids
   * @returns array proprietaires
   */
  getProprietaires( propriete_ids ): Observable <any> {

    const url             = this.globalObject.api_url + '/api/ProprieteProprietaire/getProprietaires' ;
    const body            = JSON.stringify({propriete_ids: propriete_ids});
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    return this.http.post<any>(url, body, httpOptions) ;
  }


  /**
   * Assign the Proprietes To Proprietaire
   * 
   * @param id_copropriete int - which copropriete are u in
   * @param propriete_ids mixed - array which we need to assign
   * @param proprietaire_id int - int of the table wich we will assign to
   * 
   * @returns bool assignedOrNot
   */
  assignProprietesToProprietaire( id_copropriete, propriete_ids, proprietaire_id ): Observable <any> {

    const url             = this.globalObject.api_url + '/api/ProprieteProprietaire/assignProprietesToProprietaire' ;
    const body            = JSON.stringify({id_copropriete: id_copropriete,
                                            propriete_ids: propriete_ids,
                                            proprietaire_id: proprietaire_id
                                           });
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    return this.http.post<any>(url, body, httpOptions) ;
  }


  /**
   * Assign the Proprietaires To Proprietes
   * 
   * @param int id_copropriete - which copropriete are u in
   * @param mixed proprietaire_ids - array which we need to assign
   * @param int propriete_id - int of the table wich we will assign to
   * 
   * @returns bool assignedOrNot
   */
  assignProprietairesToPropriete( id_copropriete, proprietaire_ids, propriete_id ): Observable <any> {

    const url             = this.globalObject.api_url + '/api/ProprieteProprietaire/assignProprietairesToPropriete' ;
    const body            = JSON.stringify({id_copropriete: id_copropriete,
                                            proprietaire_ids: proprietaire_ids,
                                            propriete_id: propriete_id
                                           });
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    return this.http.post<any>(url, body, httpOptions) ;
  }


  /**
   * Delete ProprieteProprietaire
   * 
   * @param int ids - ids des ProprieteProprietaire
   * 
   * @returns bool removedOrNot
   */
  DeleteProprieteProprietaire( ids ): Observable <any> {

    const url             = this.globalObject.api_url + '/api/ProprieteProprietaire/deletePermanently' ;
    const body            = JSON.stringify({id: ids});
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    return this.http.post<any>(url, body, httpOptions) ;
  }

}
