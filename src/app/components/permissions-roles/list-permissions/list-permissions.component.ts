import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute} from '@angular/router';
import { GridApi, ColumnApi, Events } from 'ag-grid';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig} from 'ng-snotify';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { GlobaleService, GlobalObject } from '../../../services/globale.service';
import { GridClass } from '../../../services/grid.class';
import { PermissionsRolesService } from '../service/permissions-roles.service' ;

@Component({
  selector: 'app-list-permissions',
  templateUrl: './list-permissions.component.html',
  styleUrls: ['./list-permissions.component.css']
})
export class ListPermissionsComponent implements OnInit {

  /*
  * Globale variables
  */
 private globalObject: GlobalObject = new GlobalObject() ;
 private rowData: any ;
 private gridApi: GridApi ;
 private gridColumnApi: ColumnApi ;
 private isColFit = true ;
 private isRowsSelected = false ;
 private getRowNodeId ;
 private msgSynchronizeSubscription ;
 private globalObjectSubscription ;
 private selectedRole ;
 private selectedGroupe ;
 private listeRoles ;
 private listeEditRoles ;
 private listeGroupes ;
 private newRole ;
 private editRole ;
 private selectedEditRole ;

 private columnDefs = [
  {headerName: 'Permissions', field: 'permission_description', width: 450},
  {headerName: '',
   suppressMenu: true,
   suppressSorting: true,
   width: 60,
   field: 'role_has_permission',
      cellRenderer : function(params) {
        return `<input type='checkbox' 
                data-action-type="role_has_permission_action"
                name="role_has_permission" 
                ${params.value==1 ? 'checked' : ''} />`;
    }
  }
];


constructor(private globaleService: GlobaleService,
           private http: HttpClient,
           private snotifyService: SnotifyService,
           private activatedroute: ActivatedRoute,
           private router: Router,
           private spinnerService: Ng4LoadingSpinnerService,
           private gridClass: GridClass,
           private permissionsRolesService: PermissionsRolesService) {
    // Subscribe to globaleService (globalObject)
    this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject()
                      .subscribe(globalObject => this.globalObject = globalObject) ;
    this.setGlobalObject() ;
}

 ngOnInit() {
   this.populateSelects();
 }

 ngOnDestroy () {
   this.globalObjectSubscription.unsubscribe();
 }

 setDataToGrid(id_role, id_cat_permission) {
  this.populatePermissionsRolesListe (id_role, id_cat_permission) ;
}

 onGridReady(params) {
  // Assign grid api to local vars
  this.gridApi = params.api;
  this.gridColumnApi = params.columnApi;
  // Fit columns for Fill screen
  this.gridApi.sizeColumnsToFit();
}


  // onRowClicked
  onRowClicked(e) {
    //  console.log('OnRowClicked', e);
      if (e.event.target !== undefined) {
        const data = e.data;
        const actionType = e.event.target.getAttribute('data-action-type');
        if (actionType == "role_has_permission_action") {
           if(data.role_has_permission == "0") {
             data.role_has_permission = "1" ;
           } else {
             data.role_has_permission = "0" ;
           }
        }
      }
   }

  onValidate() {
    let assign = new Array() ;
    let remove = new Array() ;
    let ar ;
    this.gridApi.forEachNode( function(rowNode) {
      if(rowNode.data.role_has_permission == 1)
        assign.push(rowNode.data.permission_id) ;
      if(rowNode.data.role_has_permission == 0)
        remove.push(rowNode.data.permission_id) ;
    });
    ar = {'role_id': this.selectedRole, 'assign': assign, 'remove': remove} ;

   this.spinnerService.show();
    this.permissionsRolesService.assignRoles(ar).subscribe(res=> {
      this.snotifyService.info("Permissions synchronisé", this.globalObject.configNotify);
      this.spinnerService.hide();
    });
  }
  
  onChangeGroupe() {
    if(!this.selectedRole || !this.selectedGroupe) {
      this.gridApi.setRowData([]);
      return ;
    }
    this.populatePermissionsRolesListe (this.selectedRole, this.selectedGroupe) ;
  }
  
  onChangeRole() {
    if(!this.selectedRole || !this.selectedGroupe) {
      this.gridApi.setRowData([]);
      return ;
    }
    this.populatePermissionsRolesListe (this.selectedRole, this.selectedGroupe) ;
  }

  onChangeEditRole(e) {
    this.editRole = e && e.name ? e.name : "" ;
  }

  onNewRole(myForm) {
    this.spinnerService.show();
    this.permissionsRolesService.addRole(myForm.form.value.newRole).subscribe(res=>{
      this.spinnerService.hide();
      this.newRole = "" ;

      // If error
      if(res.error)
        return ;
      this.snotifyService.success("Role ajouté", this.globalObject.configNotify);
      // Refresh selects
      this.populateSelects();
    });
  }

  onUpdateRole(myForm) {
    let f = myForm.form.value ;
    if (!f.selectedEditRole || !f.editRole) return ;
    this.spinnerService.show();
    this.permissionsRolesService.updateRole(f.selectedEditRole, f.editRole).subscribe(res=>{
      this.snotifyService.info("Role modifié", this.globalObject.configNotify);
      this.editRole = "" ;
      // Clear Edit role
      if (this.listeEditRoles.length == 0) return ;
      this.selectedEditRole = null ;
      // Refresh selects
      this.populateSelects();
      this.spinnerService.hide() ;
    });
  }

  onDeleteRole() {
    if (!this.selectedEditRole) return ;
    this.spinnerService.show();
    this.permissionsRolesService.deleteRole(this.selectedEditRole).subscribe(res=>{
      this.snotifyService.info("Role supprimé", this.globalObject.configNotify);
      this.editRole = "" ;
      // Clear Edit role
      if (this.listeEditRoles.length == 0) return ;
      this.selectedEditRole = null ;
      // Refresh selects
      this.populateSelects();
      this.spinnerService.hide() ;
    });
  }


  // Set GlobalObject
  setGlobalObject(): void {
    const globalObjectNew: GlobalObject = {
                                            'title': 'Gestion des permissions',
                                            'breadcrumb': ['Roles & Permissions']
                                          } ;
    const mergeObjects = {...this.globalObject, ...globalObjectNew} ;
    this.globaleService.setGlobalObject(mergeObjects);
  }

populateSelects() {
  this.permissionsRolesService.getRolesGroupes().subscribe(res => {
    this.listeRoles = res.roles ;
    this.listeEditRoles = res.roles ;
    this.listeGroupes = res.groupes ;
    this.defaultRolesGroupes();
    this.setDataToGrid(this.selectedRole, this.selectedGroupe);
  });
}

populatePermissionsRolesListe (id_role, id_cat_permission) {
  this.spinnerService.show();
  this.permissionsRolesService.fetch(id_role, id_cat_permission).subscribe(res => {
    this.spinnerService.hide();
    if(!res.success) return ;
    this.rowData = res.permissionsRole ;
  });
}

defaultRolesGroupes() {
  // Select 1st Groupe
  if (this.listeGroupes.length == 0) return ;
  this.selectedGroupe = this.listeGroupes[0].id ;

  // Select 1st Role
  if (this.listeRoles.length == 0) return ;
  this.selectedRole = this.listeRoles[0].id ;

   // Select 1st Edit Role
   if (this.listeEditRoles.length == 0) return ;
   this.selectedEditRole = this.listeEditRoles[0].id ;
}

}
