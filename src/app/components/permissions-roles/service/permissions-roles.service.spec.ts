import { TestBed, inject } from '@angular/core/testing';

import { PermissionsRolesService } from './permissions-roles.service';

describe('PermissionsRolesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PermissionsRolesService]
    });
  });

  it('should be created', inject([PermissionsRolesService], (service: PermissionsRolesService) => {
    expect(service).toBeTruthy();
  }));
});
