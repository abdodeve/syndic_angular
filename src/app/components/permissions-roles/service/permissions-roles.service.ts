import { Injectable, OnInit, OnDestroy } from '@angular/core';
import { GlobaleService, GlobalObject } from '../../../services/globale.service';
import { Observable } from 'rxjs' ;
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PermissionsRolesService {
 /*
  * Syncronous variable
  */
 private globalObject: GlobalObject = new GlobalObject() ;
 private coproprieteExercice ;
 private globalObjectSubscription ;


 constructor(private http: HttpClient, private globaleService: GlobaleService) {
       // Subscribe to globaleService
       this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject()
       .subscribe(globalObject => this.globalObject = globalObject) ;
  }


 ngOnInit() {

 }

 ngOnDestroy () {
   this.globalObjectSubscription.unsubscribe();
 }

 fetch(id_role, id_cat_permission): Observable <any> {

   const url             = this.globalObject.api_url + '/api/RolePermesssion/fetch' ;
   const body            = JSON.stringify({id_role: id_role,
                                          id_cat_permission: id_cat_permission});
   const httpOptions = {
                         headers: new HttpHeaders({
                           'Accept'        : 'application/json',
                           'Content-Type'  : 'application/json',
                           'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                         })
                       };
    return this.http.post<any>(url, body, httpOptions) ;
 }

 getRolesGroupes(): Observable <any> {

  const url         = this.globalObject.api_url + '/api/RolePermesssion/getRolesGroupes' ;
  const httpOptions = {
                        headers: new HttpHeaders({
                          'Accept'        : 'application/json',
                          'Content-Type'  : 'application/json',
                          'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                        })
                      };
  return this.http.post<any>(url, null, httpOptions) ;
}

assignRoles(sync): Observable <any> {

  const url         = this.globalObject.api_url + '/api/RolePermesssion/assign_permissions_to_role' ;
  const body            = JSON.stringify({role_id: sync.role_id,
                                          remove: sync.remove,
                                          assign: sync.assign
                                         });
  const httpOptions = {
                        headers: new HttpHeaders({
                          'Accept'        : 'application/json',
                          'Content-Type'  : 'application/json',
                          'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                        })
                      };
  return this.http.post<any>(url, body, httpOptions) ;
}

addRole(name): Observable <any> {

  const url         = this.globalObject.api_url + '/api/RolePermesssion/addRole' ;
  const body            = JSON.stringify({ name: name });
  const httpOptions = {
                        headers: new HttpHeaders({
                          'Accept'        : 'application/json',
                          'Content-Type'  : 'application/json',
                          'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                        })
                      };
  return this.http.post<any>(url, body, httpOptions) ;
}

updateRole(id, name): Observable <any> {

  const url         = this.globalObject.api_url + '/api/RolePermesssion/updateRole' ;
  const body            = JSON.stringify({
                                          id: id,
                                          name: name
                                         });
  const httpOptions = {
                        headers: new HttpHeaders({
                          'Accept'        : 'application/json',
                          'Content-Type'  : 'application/json',
                          'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                        })
                      };
  return this.http.post<any>(url, body, httpOptions) ;
}

deleteRole(id): Observable <any> {

  const url         = this.globalObject.api_url + '/api/RolePermesssion/deleteRole' ;
  const body            = JSON.stringify({
                                          id: id
                                         });
  const httpOptions = {
                        headers: new HttpHeaders({
                          'Accept'        : 'application/json',
                          'Content-Type'  : 'application/json',
                          'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                        })
                      };
  return this.http.post<any>(url, body, httpOptions) ;
}

}
