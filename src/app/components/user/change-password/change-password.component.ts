import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms' ;
import { GlobaleService, GlobalObject } from '../../../services/globale.service';
import { Observable } from 'rxjs' ;
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {SnotifyService, SnotifyPosition, SnotifyToastConfig} from 'ng-snotify';

declare var $: any;

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit, OnDestroy {

  /*
  * Syncronous variable
  */
  globalObject: GlobalObject = new GlobalObject() ;
  private globalObjectSubscription ;

  configNotify = {'position': 'rightTop'} ;

  constructor(private http: HttpClient,
              private globaleService: GlobaleService,
              private snotifyService: SnotifyService) {
    // Subscribe to globaleService
    this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject()
    .subscribe(globalObject => this.globalObject = globalObject) ;
   }

  ngOnInit() {

  }

  ngOnDestroy () {
    this.globalObjectSubscription.unsubscribe();
  }

  valider( form: NgForm ) {
    const url             = this.globalObject.api_url + '/api/changePassword' ;
    const body            = JSON.stringify({
              'currentPassword'  : form.value.currentPassword ,
              'newPassword'      : form.value.newPassword,
              'repeatPassword'   : form.value.repeatPassword
            });
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    this.http.post<any>(url, body, httpOptions).subscribe(res => {
        switch (res.error) {
            case 'incorrect_password': {
                this.snotifyService.error(`Vérifier mot de passe actuel`, this.configNotify);
                return ;
            }
            case 'not_match': {
                this.snotifyService.error(`Mots de passe ne correspond pas`, this.configNotify);
                return ;
            }
        }

        this.snotifyService.success(`Mot de passe est changé`, this.configNotify);
        $('#changePassword-modal').modal('hide') ;
        form.reset() ;
     }) ;
  }

}
