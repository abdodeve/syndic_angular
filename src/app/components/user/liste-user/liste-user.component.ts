import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Router, ActivatedRoute} from '@angular/router';
import { AgGridNg2 } from 'ag-grid-angular';
import { GridApi, ColumnApi, Events } from 'ag-grid';
import {SnotifyService, SnotifyPosition, SnotifyToastConfig} from 'ng-snotify';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { GlobaleService, GlobalObject } from '../../../services/globale.service';
import { User } from '../user' ;
import { UserService } from '../service/user.service';
import { GridClass } from '../../../services/grid.class' ;

declare const Echo: any;
@Component({
  selector: 'app-liste-user',
  templateUrl: './liste-user.component.html',
  styleUrls: ['./liste-user.component.css']
})
export class ListeUserComponent implements OnInit, OnDestroy {

  /*
  * Globale variables
  */
  private globalObject: GlobalObject = new GlobalObject() ;
  private columnDefs = [
    {headerName: 'Nom', field: 'name', checkboxSelection: true },
    {headerName: 'E-Mail', field: 'email' },
    {headerName: 'Role', field: 'role_name' },
    {
      headerName: 'Action',
      suppressMenu: true,
      suppressSorting: true,
      width: 80,
      template: `
                <button type="button" data-action-type="edit"
                  class="btn btn-icon waves-effect waves-light btn-info" title="editer">
                  <i class="fa fa-edit" data-action-type="edit"></i>
                </button>

                <button type="button" data-action-type="delete"
                  class="btn btn-icon waves-effect waves-light btn-danger" title="supprimer">
                  <i class="fa fa-trash" data-action-type="delete"></i>
                </button>
                `
    }
  ];
  private rowData: any ;
  private gridApi: GridApi ;
  private gridColumnApi: ColumnApi ;
  private isColFit = true ;
  private isRowsSelected = false ;
  private getRowNodeId ;
  private msgSynchronizeSubscription ;
  private globalObjectSubscription ;
  private coproprieteExercice ;
  private coproprieteExerciceSubscription ;

constructor(private globaleService: GlobaleService,
            private http: HttpClient,
            private userService: UserService,
            private snotifyService: SnotifyService,
            private activatedroute: ActivatedRoute,
            private router: Router,
            private spinnerService: Ng4LoadingSpinnerService,
            private gridClass: GridClass) {
          // Subscribe to globaleService (globalObject)
          this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject()
                            .subscribe(globalObject => this.globalObject = globalObject) ;
          this.setGlobalObject() ;
          // Get RowNodeId
          this.getRowNodeId = function(data) {
            if (data) return data.id;
          };
   // this.synchronize();
 }

  ngOnInit() {
    this.setDataToGrid();
  }

  ngOnDestroy () {
    this.globalObjectSubscription.unsubscribe();
  }

  setDataToGrid() {
    this.spinnerService.show();
    this.userService.fetch().subscribe(res => {
      this.rowData = res.users ;
      this.spinnerService.hide();
    });
  }

  onGridReady(params) {
    // Assign grid api to local vars
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    // Fit columns for Fill screen
    this.gridApi.sizeColumnsToFit();
  }

  // Set GlobalObject
  setGlobalObject(): void {
    const globalObjectNew: GlobalObject = {
                                           'title': 'Liste utilisateur',
                                           'breadcrumb': ['Utilisateur', 'Liste']
                                          } ;
    const mergeObjects = {...this.globalObject, ...globalObjectNew} ;
    this.globaleService.setGlobalObject(mergeObjects);
  }

  // If more than 1 row selected
  // Set this.isRowsSelected to true
  // For Display btn 'Supprimer sélections'
  onRowSelected(event) {
    const selectedRows = this.gridApi.getSelectedRows();
    if (selectedRows.length > 1) {
      this.isRowsSelected = true ;
    } else {
      this.isRowsSelected = false ;
    }
  }

  // onDoubleRowClicked autoSize/fitSize
  onDoubleRowClicked(event) {
    if (this.isColFit) {
      this.gridColumnApi.autoSizeAllColumns();
      this.isColFit = false ;
    } else {
      this.gridApi.sizeColumnsToFit();
      this.isColFit = true ;
    }
  }

  // onRowClicked
  // Button Edit & Delete
  onRowClicked(e) {
    if (e.event.target !== undefined) {
      const data = e.data;
      const actionType = e.event.target.getAttribute('data-action-type');
      switch (actionType) {
          case 'edit':
              return this.onActionEditClick(data);
          case 'delete':
              return this.onActionDeleteClick(data);
      }
    }
  }

  // Filter Grid onKeyp
  onKeyp(event) {
    this.gridApi.setQuickFilter(event.target.value);
  }

  // Action Edit
  onActionEditClick(data: any) {
   //  console.log('View action clicked', data);
    this.router.navigate(['/home/user/edit/' + data.id]);
  }

  // Action Delete
  onActionDeleteClick(data: any) {
    this.snotifyService.confirm('Êtes-vous sûr de vouloir supprimer ce utilisateur ?', {
      timeout: 8000,
      position: 'centerTop',
      buttons: [
        {text: 'Oui', action: () => this.deleteRow(data.id), bold: false},
        {text: 'Non', action: (toast) => this.snotifyService.remove(toast.id)},
        {text: 'Fermer', action: (toast) => {this.snotifyService.remove(toast.id); }, bold: true},
      ]
    });
  }

  deleteRow(id) {
    const singularAndPlural = {'singular': 'Utilisateur', 'plural': 'Utilisateurs'} ;  
    this.snotifyService.clear();
    this.spinnerService.show();
    // Delte request
    this.userService.delete(id).subscribe(res => {
        this.gridClass.deleteFromGrid(this.gridApi ,res.deletedPermanently_id, singularAndPlural);
        this.spinnerService.hide();
    });
  }

  onDeleteSeletedRows() {
   this.snotifyService.confirm('Êtes-vous sûr de vouloir supprimer ces utilisateurs ?', {
      timeout: 8000,
      position: 'centerTop',
      buttons: [
        {text: 'Oui', action: () => this.deleteRows(), bold: false},
        {text: 'Non', action: (toast) => this.snotifyService.remove(toast.id)},
        {text: 'Fermer', action: (toast) => {this.snotifyService.remove(toast.id); }, bold: true},
      ]
    });
  }

  deleteRows () {
    const ids = new Array() ;
    const singularAndPlural = {'singular': 'Utilisateur', 'plural': 'Utilisateurs'} ;

    this.snotifyService.clear();
    this.spinnerService.show();
    this.gridApi.getSelectedRows().forEach(element => { ids.push(element.id) });
    // Delete Request
    this.userService.delete(ids).subscribe(res => {
        this.gridClass.deleteFromGrid(this.gridApi, res.deletedPermanently_id, singularAndPlural);
        this.isRowsSelected = false ;
        this.spinnerService.hide() ;
    });
  }

}
