import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm, Form } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Router, ActivatedRoute} from '@angular/router';
import {SnotifyService, SnotifyPosition, SnotifyToastConfig} from 'ng-snotify';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { GlobaleService, GlobalObject } from '../../../services/globale.service';
import { User } from '../user' ;
import { UserService } from '../service/user.service';
import { TopbarService } from '../../topbar/service/topbar.service' ;

declare const Echo: any;
@Component({
  selector: 'app-maj-user',
  templateUrl: './maj-user.component.html',
  styleUrls: ['./maj-user.component.css']
})
export class MajUserComponent  implements OnInit, OnDestroy {

  /*
  * Globale variables
  */

 private globalObject: GlobalObject = new GlobalObject() ;
 private user: User = {
                          name: null,
                          email: null,
                          password: null
                      };
 private id ;
 private isNew = true ;
 private next ;
 private previous ;
 private globalObjectSubscription ;
 private copropriete ;
 private listeRoles ;
 private selectedRole ;

 constructor(private globaleService: GlobaleService,
             private userService: UserService,
             private http: HttpClient,
             private snotifyService: SnotifyService,
             private activatedroute: ActivatedRoute,
             private router: Router,
             private spinnerService: Ng4LoadingSpinnerService,
             private topbarService: TopbarService
           ) {

    // Subscribe to globaleService (globalObject)
    this.globalObjectSubscription =  this.globaleService
                                    .get_Observable_GlobalObject()
                                    .subscribe(globalObject => this.globalObject = globalObject) ;
  }

  ngOnInit(): void {
    this.populateSelect();
    this.NewOrEdit();
    this.setGlobalObject();
  }

  ngOnDestroy () {
    this.globalObjectSubscription.unsubscribe();
  }

  setGlobalObject(): void {
    let globalObjectNew: GlobalObject ;
    // Check if you are on New or Edit
    if (this.isNew) {
          globalObjectNew = {
                              'title': 'Nouveau utilisateur',
                              'breadcrumb': ['Utilisateur', 'Nouveau']
                            } ;
    } else {
          globalObjectNew = {
                              'title': 'Editer utilisateur',
                              'breadcrumb': ['Utilisateur', 'Editer']
                            } ;
    }
    const mergeObjects = {...this.globalObject, ...globalObjectNew} ;
    this.globaleService.setGlobalObject(mergeObjects);
  }

  onValidate(form: NgForm) {

    this.spinnerService.show();
    if (form.invalid) {
      return;
    }
    if ( this.isNew ) {
      this.userService.insert( this.user ).subscribe( data => {
        this.spinnerService.hide();

        if(data.error == 'user_exist') {
          this.snotifyService.warning('Email exist !', this.globalObject.configNotify) ;
          return ;
        }

        this.snotifyService.success('Nouvelle utilisateur ajouter', this.globalObject.configNotify) ;
        this.router.navigate(['/home/user/liste']);
      });
    } else {
      this.userService.update( this.id, this.user ).subscribe( data => {
        this.spinnerService.hide();
        
        if(data.error == 'user_exist') {
          this.snotifyService.warning('Email exist !', this.globalObject.configNotify) ;
          return ;
        }

        this.snotifyService.info('Utilisateur modifier', this.globalObject.configNotify) ;
        this.router.navigate(['/home/user/liste']);
      });
    }
  }

  onDelete() {
    this.snotifyService.confirm('Êtes-vous sûr de vouloir supprimer ce utilisateur ?', {
      timeout: 8000,
      position: 'centerTop',
      buttons: [
        {text: 'Oui', action: () => this.deleteConfirmed(this.id), bold: false},
        {text: 'Non', action: (toast) => this.snotifyService.remove(toast.id)},
        {text: 'Fermer', action: (toast) => {this.snotifyService.remove(toast.id); }, bold: true},
      ]
    });
  }

  onCancel() {
    this.router.navigate(['/home/user/liste']);
  }

  single() {
    this.spinnerService.show();
    this.userService.single(this.id).subscribe( res => {

        // If id not found
        if(res.error == "not_found")   {
          this.snotifyService.warning('Utilisateur introuvable !', this.globalObject.configNotify);
          this.router.navigate(['/home/user/liste']);
          return ;
        }
        
        this.user = res.user ;
        this.next = res.next ;
        this.previous = res.previous ;
        this.spinnerService.hide();
    } );
  }

  NewOrEdit() {
    this.activatedroute.params.subscribe(params => {
      this.id = params['id'] ;
        if (this.id) {
            this.isNew = false ;
            this.single();
        }
    });
  }

  deleteConfirmed(id) {
    this.snotifyService.clear();
    this.spinnerService.show();
    this.userService.delete(id).subscribe(res => {
          this.spinnerService.hide();
          this.snotifyService.info('Utilisateur supprimer', this.globalObject.configNotify) ;
          this.router.navigate(['/home/user/liste']);
     });
  }

  isEnd(nextOrPrevious) {
    if (nextOrPrevious === 'next') {
      return this.next == null ? true : false ;
    }
    if (nextOrPrevious === 'previous') {
      return this.previous == null ? true : false ;
    }
  }

  populateSelect() {
    this.userService.getRoles().subscribe(res => {
      this.listeRoles = res.roles ;
    });
  }

}
