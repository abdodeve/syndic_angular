import { Injectable, OnInit, OnDestroy } from '@angular/core';
import { GlobaleService, GlobalObject } from '../../../services/globale.service';
import { Observable } from 'rxjs' ;
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../user';

@Injectable({
  providedIn: 'root'
})
export class UserService implements OnInit, OnDestroy {


  /*
  * Syncronous variable
  */
  private globalObject: GlobalObject = new GlobalObject() ;
  private globalObjectSubscription ;

  constructor(private http: HttpClient, private globaleService: GlobaleService) {
        // Subscribe to globaleService
        this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject()
        .subscribe(globalObject => this.globalObject = globalObject) ;
   }


  ngOnInit() {

  }

  ngOnDestroy () {
    this.globalObjectSubscription.unsubscribe();
  }

  fetch(): Observable <any> {

    const url             = this.globalObject.api_url + '/api/user/fetch' ;
    const body            = null ;
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    return this.http.post<User>(url, body, httpOptions) ;
  }

  single( id ): Observable <any> {
    const url             = this.globalObject.api_url + '/api/user/single/' + id ;
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    return this.http.get<any>(url, httpOptions) ;
  }

  insert( user: User ): Observable <any> {

    const url             = this.globalObject.api_url + '/api/user/insert' ;
    const body            = JSON.stringify(user);
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token')),
                          })
                        };
    return this.http.post<User>(url, body, httpOptions) ;
  }

  update( id, user: User ): Observable <any> {

    const url             = this.globalObject.api_url + '/api/user/update/' + id ;
    const body            = JSON.stringify(user);
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    return this.http.put<User>(url, body, httpOptions) ;
  }

  delete( id ): Observable <any> {

    const url             = this.globalObject.api_url + '/api/user/deletePermanently' ;
    const body            = JSON.stringify( {'id': id} );
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    return this.http.post<any>(url, body, httpOptions) ;
  }

  getRoles(): Observable <any> {

    const url         = this.globalObject.api_url + '/api/RolePermesssion/getRoles' ;
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    return this.http.post<any>(url, null, httpOptions) ;
  }

  /**
   * Sign up new user
   * This function used in: register-modal Component
   * @param form 
   */
  signUp( form ): Observable <any> {

    const url             = this.globalObject.api_url + '/api/signUp' ;
    const body            = JSON.stringify({
                              'email'     : form.email,
                              'password'  : form.password,
                              'name'      : form.name
                            });
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json'
                          })
                        };
    return this.http.post<any>(url, body, httpOptions);
  }



}
