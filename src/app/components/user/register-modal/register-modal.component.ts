import { Component, OnInit, OnDestroy} from '@angular/core';
import { UserService } from '../service/user.service' ;
import { NgForm } from '@angular/forms' ;
import { GlobaleService, GlobalObject } from '../../../services/globale.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SnotifyService } from 'ng-snotify';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

declare var $: any;

@Component({
  selector: 'app-register-modal',
  templateUrl: './register-modal.component.html',
  styleUrls: ['./register-modal.component.css']
})
export class RegisterModalComponent implements OnInit, OnDestroy {

  /*
  * Syncronous variable
  */
  globalObject: GlobalObject = new GlobalObject() ;
  private globalObjectSubscription ;
  private termsCheckbox ;

  constructor(private http: HttpClient,
              private globaleService: GlobaleService,
              private snotifyService: SnotifyService,
              private spinnerService: Ng4LoadingSpinnerService,
              private userService: UserService) {

    // Subscribe to globaleService
    this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject()
    .subscribe(globalObject => this.globalObject = globalObject) ;
   }

  ngOnInit() {
  }

  ngOnDestroy () {
    this.globalObjectSubscription.unsubscribe();
  }

  /**
   * signUp Event click
   * 
   * @param form 
   */
  signUp ( form: NgForm ): void {
    console.log(form.value);
    this.spinnerService.show();

    this.userService.signUp( form.value ).subscribe(res => {
    this.spinnerService.hide();

    if (res.error == 'user_exist') {
      this.snotifyService.warning(`Utilisateur existe déja`, this.globalObject.configNotify);
      return ;
    }
    this.snotifyService.success(`Merci! Votre inscription est éffectué, Votre compte sera bientôt activer`, this.globalObject.configNotify);
    $('#signup-modal').modal('hide') ;

     form.reset() ;
    }) ;
   }
}
