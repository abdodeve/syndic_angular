import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms' ;
import { GlobaleService, GlobalObject } from '../../../services/globale.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {SnotifyService} from 'ng-snotify';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

declare var $: any;

@Component({
  selector: 'app-reset-passowrd',
  templateUrl: './reset-passowrd.component.html',
  styleUrls: ['./reset-passowrd.component.css']
})
export class ResetPassowrdComponent implements OnInit, OnDestroy {

  /*
  * Syncronous variable
  */
  globalObject: GlobalObject = new GlobalObject() ;
  private globalObjectSubscription ;

  configNotify = {'position': 'rightTop'} ;

  constructor(private http: HttpClient,
              private globaleService: GlobaleService,
              private snotifyService: SnotifyService,
              private spinnerService: Ng4LoadingSpinnerService) {

    // Subscribe to globaleService
    this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject()
    .subscribe(globalObject => this.globalObject = globalObject) ;
   }

  ngOnInit() {
  }

  ngOnDestroy () {
    this.globalObjectSubscription.unsubscribe();
  }

  send ( form: NgForm ): void {
    
    this.spinnerService.show();
    const url             = this.globalObject.api_url + '/api/forgot/password' ;
    const body            = JSON.stringify({
                              'email'  : form.value.resetEmail
                            });
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json'
                          })
                        };
                        
    this.http.post<any>(url, body, httpOptions).subscribe(res => {
        this.spinnerService.hide();
        if( res.error ){
          this.snotifyService.error(`Email n'existe pas`, this.configNotify);
          return ;
        }

        this.snotifyService.success(`Opération éffectué, consulter votre boite email`, this.configNotify);
        $('#resetPassword-modal').modal('hide') ;
        form.reset() ;
      }) ;
   }
}
