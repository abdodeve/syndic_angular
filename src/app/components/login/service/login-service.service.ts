import { Injectable, OnDestroy } from '@angular/core';
import { GlobaleService, GlobalObject } from '../../../services/globale.service';
import { Observable } from 'rxjs' ;
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, retry } from 'rxjs/operators';
// import { ErrorObservable } from 'rxjs';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Router } from '@angular/router';


@Injectable()
export class LoginServiceService implements OnDestroy {


  /*
  * Syncronous variable
  */
  private globalObject: GlobalObject ;
  private globalObjectSubscription ;

  constructor(private http: HttpClient, 
              private globaleService: GlobaleService,
              private spinnerService: Ng4LoadingSpinnerService,
              private router: Router) {
   // Subscribe to globaleService
   this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject()
                      .subscribe(globalObject => this.globalObject = globalObject) ;
}

    ngOnDestroy () {
      this.globalObjectSubscription.unsubscribe();
    }

   login( form: any ): Observable <any> {
    const url           = this.globalObject.api_url + '/api/login' ;
    /*
    const client_id     = this.globalObject.client_id ;
    const client_secret = this.globalObject.client_secret ;
     'client_id'         : client_id,
    'client_secret'     : client_secret,
    'grant_type'        : 'password', 
    */
    const body    = JSON.stringify({
      'email'             : form.username ,
      'password'          : form.password
    });
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept':  'application/json',
                            'Content-Type': 'application/json'
                          })
                        };
    return this.http.post<any>(url, body, httpOptions);
  }


   refresh(): Observable <any> {
 //   async refresh() {

    const url             = this.globalObject.api_url + '/oauth/token' ;
    const refresh_token   = this.globalObject.refresh_token ;
    const client_id       = this.globalObject.client_id ;
    const client_secret   = this.globalObject.client_secret ;
    const body            = JSON.stringify({
              'client_id'         : this.globalObject.client_id,
              'client_secret'     : this.globalObject.client_secret,
              'grant_type'        : 'refresh_token',
              'refresh_token'     : JSON.parse(localStorage.getItem('refresh_token'))
            });
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept':  'application/json',
                            'Content-Type': 'application/json'
                          })
                        };
    return this.http.post<any>(url, body, httpOptions) ;
  }

  userLoggedIn(): Observable <any> {

    const url         = this.globalObject.api_url + '/api/user/loggedIn' ;
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    return this.http.get<any>(url, httpOptions) ;
  }

  fetch( id_copropriete = 1 ): Observable <any> {

    const url             = this.globalObject.api_url + '/api/proprietaire/fetch' ;
    const body            = JSON.stringify({id_copropriete: id_copropriete});
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    return this.http.post<any>(url, body, httpOptions) ;
  }

  logout(): Observable <any> {
    const user = {'user_id': this.globalObject.user_id} ;
    const url         = this.globalObject.api_url + '/api/user/logout' ;
    const body            = JSON.stringify(user);
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    return this.http.post<any>(url, body, httpOptions);
  }

  /////////////////////// Methodes ////////////////////////////

  logoutAction() {
    this.spinnerService.show();
    this.logout().subscribe(res => {
      this.spinnerService.hide();
      // Unset LocalStorages
      this.globaleService.unsetLocalAuth() ;
      this.router.navigate(['/login']);
    });

    // Clear Interval
    clearInterval(this.globalObject.timer);
    // Clear timer+lastActive GlobalObject
    this.globaleService.setGlobalObject({...this.globalObject, ...{'timer': null}});
    localStorage.removeItem('lastActive');
  }

}
