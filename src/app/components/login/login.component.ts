import { Component, OnInit, OnDestroy } from '@angular/core';
import { GlobaleService, GlobalObject } from '../../services/globale.service';
import { NgForm } from '@angular/forms';
import { LoginServiceService } from './service/login-service.service' ;
import { HttpClient, HttpHeaders, HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs' ;

import { Router } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {SnotifyService, SnotifyPosition, SnotifyToastConfig} from 'ng-snotify';
import { UserIdleService } from '../../services/user-idle/user-idle.service';




@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy   {

  /*
  * Globale variable
  */
  private globalObject: GlobalObject = new GlobalObject() ;

  private username: string ;
  private password: string ;
  private loginResults ;
  private timer ;
  private globalObjectSubscription ;

 constructor(
             private globaleService: GlobaleService,
             private loginServiceService: LoginServiceService,
             private http: HttpClient,
             private router: Router,
             private spinnerService: Ng4LoadingSpinnerService,
             private snotifyService: SnotifyService,
             private userIdleService: UserIdleService,
            ) {

      // Subscribe to globaleService
      this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject()
                         .subscribe(globalObject => this.globalObject = globalObject) ;
 }

 ngOnDestroy () {
    this.globalObjectSubscription.unsubscribe();
 }

  ngOnInit() {

    if ( JSON.parse(localStorage.getItem('isLoggedIn')) === true && <string> this.router.url === '/login') {
        console.log('You are logged in - I ll Redirect you to Home');
        this.router.navigate(['/home']);
    }

  }

    onLogin(form: NgForm) {
     this.spinnerService.show();
     this.loginServiceService.login( form.value ).subscribe(res => {
      this.spinnerService.hide();

      if( res.error == "invalid_credentials" ){
        this.snotifyService.error('Login et/ou mot de passe est incorrecte', this.globalObject.configNotify);
        return ;
      }

          // Set globalObject
          const globalObjectNew: GlobalObject = {
                                                  'access_token': res.access_token ,
                                                  'isLoggedIn'  : true
                                                } ;
         const mergeObjects = {...this.globalObject, ...globalObjectNew} ;
         this.globaleService.setGlobalObject(mergeObjects);
       //  this.timer = setInterval(() => this.refreshToken() , 1 * 10 * 1000);

         // Store tokens in localStorage
         this.globaleService.setLocalAuth() ;

        // Launch CheckInactivity Interval
        this.userIdleService.startWatching();

         this.globalObjectSubscription.unsubscribe();

         this.router.navigate(['/home']);
      }, error => {
          this.snotifyService.error('Login et/ou mot de passe est incorrecte', this.globalObject.configNotify);
          this.spinnerService.hide();
      });
 }

}
