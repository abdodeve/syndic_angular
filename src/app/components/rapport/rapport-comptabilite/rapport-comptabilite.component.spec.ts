import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RapportComptabiliteComponent } from './rapport-comptabilite.component';

describe('RapportComptabiliteComponent', () => {
  let component: RapportComptabiliteComponent;
  let fixture: ComponentFixture<RapportComptabiliteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RapportComptabiliteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RapportComptabiliteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
