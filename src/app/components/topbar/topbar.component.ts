import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, AfterViewInit, OnDestroy} from '@angular/core';
import { GlobaleService, GlobalObject } from '../../services/globale.service';
import { Router } from '@angular/router';
import { LoginServiceService } from '../login/service/login-service.service' ;
import { TopbarService } from './service/topbar.service' ;
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NgxPermissionsService } from 'ngx-permissions';

declare var $: any;

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit, AfterViewInit, OnDestroy {


  /*
  * Globale variable
  */
  private globalObject: GlobalObject ;
  private listeCoproprietes: any[] ;
  private listeExercice ;
  private listeCoproprietesExercice ;
  private selectedCopropriete ;
  private selectedExercice ;
  private globalObjectSubscription ;
  private coproprieteExerciceSubscription ;


  constructor(private globaleService: GlobaleService,
              private router: Router,
              private http: HttpClient,
              private loginService: LoginServiceService,
              private topbarService: TopbarService,
              private spinnerService: Ng4LoadingSpinnerService,
              private permissionsService: NgxPermissionsService,
            ) {
                  // Subscribe to globaleService (globalObject)
                this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject()
                       .subscribe(globalObject => {
                          this.globalObject = globalObject ;
                      }) ;

                // Subscribe to CoproprieteExercice
                this.coproprieteExerciceSubscription = this.globaleService.get_Observable_CoproprieteExercice().subscribe(res => {
                  if (!res) {
                    return ;
                  }
                  this.selectedCopropriete = res.coporpriete_primaire_id ;
                  this.selectedExercice = res.exercice ;
                });
        }

  ngOnInit() {
        // Polpulate Copropriete & Exercice
        this.populateCoproprieteExercice();

        // Load Permissions
         this.topbarService.loadPermission();
  }

  ngOnDestroy () {
    this.globalObjectSubscription.unsubscribe();
    this.coproprieteExerciceSubscription.unsubscribe();
  }

  ngAfterViewInit() {
  }

  onLogout() {
    this.spinnerService.show();
    this.loginService.logoutAction();
    // this.loginServiceService.logout().subscribe(res => {
    //         this.spinnerService.hide();
    //         // Unset LocalStorages
    //         this.globaleService.unsetLocalAuth() ;
    //         this.router.navigate(['/login']);
    // }) ;
  }

  mytest() {
    var date = new Date(JSON.parse(localStorage.getItem('lastActive')));
    console.log(date);
  }

  populateCoproprieteExercice() {
    // Subscribe to CoproprieteExercice
    this.spinnerService.show();
    this.topbarService.getCoproprietesExercices().subscribe(res => {
      this.spinnerService.hide();
      this.listeCoproprietes = res['coproprietes_primaire'] ;
      this.listeCoproprietesExercice = res['coproprietes_exercices'] ;
      this.defaultCoproprieteExercice() ;
      this.setCoproprieteExercice() ;
    });
  }

  onChangeCopropriete(event = null) {

    if (!this.selectedCopropriete) {
      return ;
    }
    // Search selected Copropriete & populate listeExercice
    this.listeExercice = this.listeCoproprietesExercice.filter(elem =>
      elem.id_copropriete_primaire === this.selectedCopropriete
      );
    // Select last exercice
    this.selectedExercice = this.listeExercice[this.listeExercice.length - 1].exercice ;
    // Search selected Copropriete
    this.setCoproprieteExercice();
  }

  onChangeExercice(event) {
    if (!this.selectedExercice) {
      return ;
    }
    // Search selected Copropriete
    this.setCoproprieteExercice();
  }

  setCoproprieteExercice () {

    if (this.listeCoproprietes.length == 0) {
      return ;
    }
      // Get Nom Copropriete
      const nomCopropriete = this.listeExercice[0].nom ;
      const idCopropriete = this.getIDCopropriete();

      const coproprieteExercice = {
            'copropriete_id' : idCopropriete ,
            'copropriete_primaire_nom' : nomCopropriete,
            'coporpriete_primaire_id': this.selectedCopropriete,
            'exercice': this.selectedExercice
            } ;
      this.globaleService.setCoproprieteExercice(coproprieteExercice);
  }

  defaultCoproprieteExercice () {
    // Select last copropriete
    if (this.listeCoproprietes.length == 0) {
      return ;
    }

    const lastCopropriete = this.listeCoproprietes[this.listeCoproprietes.length - 1]
                            .id_copropriete_primaire ;
    this.selectedCopropriete = lastCopropriete ;
    // Filter and search last exercice
    this.listeExercice = this.listeCoproprietesExercice.filter(elem =>
      elem.id_copropriete_primaire === this.selectedCopropriete
      );
    // Select last exercice
    this.selectedExercice = this.listeExercice[this.listeExercice.length - 1].exercice ;
  }

  getIDCopropriete () {

    if (!this.listeCoproprietesExercice) { return ; }

    // Filter listeCoproprietesExercice by selectedCopropriete (first filter 1)
    const filtreByCoproprietePrimaire = this.listeCoproprietesExercice.filter(elem =>
      elem.id_copropriete_primaire === this.selectedCopropriete
    );

    // Filter listeCoproprietesExercice by selectedExercice (seconde filter 2)
    const filtreByExercice  = filtreByCoproprietePrimaire.filter(elem =>
      elem.exercice === this.selectedExercice
    );

    return filtreByExercice[0].id_copropriete ;
  }

}
