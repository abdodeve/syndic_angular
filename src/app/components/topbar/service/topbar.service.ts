import { Injectable, OnDestroy } from '@angular/core';
import { GlobaleService, GlobalObject } from '../../../services/globale.service';
import { Observable } from 'rxjs' ;
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgxPermissionsService } from 'ngx-permissions';


@Injectable({
  providedIn: 'root'
})
export class TopbarService implements OnDestroy {

  /*
  * Syncronous variable
  */
  private globalObject: GlobalObject = new GlobalObject() ;
  private globalObjectSubscription ;

  constructor(private http: HttpClient,
              private globaleService: GlobaleService,
              private permissionsService: NgxPermissionsService) {
      // Subscribe to globaleService
      this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject()
                                      .subscribe(globalObject => this.globalObject = globalObject) ;
  }

  ngOnDestroy () {
    this.globalObjectSubscription.unsubscribe();
  }

  getCoproprietesExercices(): Observable <any> {

    const url          = this.globalObject.api_url + '/api/copropriete/coproprietesExercices' ;
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
                        
    return this.http.get<any>(url, httpOptions) ;
  }
  
  getPermissions(): Observable <any> {
    const url             = this.globalObject.api_url + '/api/RolePermesssion/getPermissions' ;
    const httpOptions = {
                          headers: new HttpHeaders({
                            'Accept'        : 'application/json',
                            'Content-Type'  : 'application/json',
                            'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem('access_token'))
                          })
                        };
    return this.http.post<any>(url, null, httpOptions);
  }

   loadPermission() {
     let perms = JSON.parse(localStorage.getItem('permissions'));
    // if(localStorage.getItem('permissions'))
    if(perms)
      this.permissionsService.loadPermissions(JSON.parse(localStorage.getItem('permissions')));
    else
      localStorage.setItem('permissions', JSON.stringify(null)) ;

    this.getPermissions().subscribe(res=> {
      this.permissionsService.loadPermissions(res.permissions);
      localStorage.setItem('permissions', JSON.stringify(res.permissions));
      console.log(res.permissions);
     });
  }

}
