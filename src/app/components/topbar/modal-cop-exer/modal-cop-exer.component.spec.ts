import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCopExerComponent } from './modal-cop-exer.component';

describe('ModalCopExerComponent', () => {
  let component: ModalCopExerComponent;
  let fixture: ComponentFixture<ModalCopExerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCopExerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCopExerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
