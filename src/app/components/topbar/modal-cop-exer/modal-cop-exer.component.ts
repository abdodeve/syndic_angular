import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms' ;
import { GlobaleService, GlobalObject } from '../../../services/globale.service';
import { Observable } from 'rxjs' ;
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {SnotifyService, SnotifyPosition, SnotifyToastConfig} from 'ng-snotify';
import { TopbarService } from '../service/topbar.service' ;

declare var $: any;


@Component({
  selector: 'app-modal-cop-exer',
  templateUrl: './modal-cop-exer.component.html',
  styleUrls: ['./modal-cop-exer.component.css']
})
export class ModalCopExerComponent  implements OnInit, OnDestroy {

  /*
  * Globale variable
  */
  private globalObject: GlobalObject ;
  private listeCoproprietes: any[] ;
  private listeExercice ;
  private listeCoproprietesExercice ;
  private selectedCopropriete ;
  private selectedExercice ;
  private globalObjectSubscription ;
  private coproprieteExerciceSubscription ;

  configNotify = {'position': 'rightTop'} ;

  constructor(private http: HttpClient,
              private globaleService: GlobaleService,
              private snotifyService: SnotifyService,
              private topbarService: TopbarService) {
      // Subscribe to globaleService
      this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject()
      .subscribe(globalObject => this.globalObject = globalObject) ;

      // Subscribe to CoproprieteExercice
      this.populateCoproprieteExercice();

      // Subscribe to CoproprieteExercice
      this.coproprieteExerciceSubscription = this.globaleService.get_Observable_CoproprieteExercice().subscribe(res => {
        if (!res) {
          return ;
        }
        this.selectedCopropriete = res.coporpriete_primaire_id ;
        this.selectedExercice = res.exercice ;
      });
   }

  ngOnInit() {

  }

  ngOnDestroy () {
    this.globalObjectSubscription.unsubscribe();
    this.coproprieteExerciceSubscription.unsubscribe();
  }

  populateCoproprieteExercice() {
    // Subscribe to CoproprieteExercice
    this.topbarService.getCoproprietesExercices().subscribe(res => {
      this.listeCoproprietes = res['coproprietes_primaire'] ;
      this.listeCoproprietesExercice = res['coproprietes_exercices'] ;
      this.defaultCoproprieteExercice() ;
      this.setCoproprieteExercice() ;
    });
  }

  onChangeCopropriete(event = null) {

    if (!this.selectedCopropriete) {
      return ;
    }
    // Search selected Copropriete & populate listeExercice
    this.listeExercice = this.listeCoproprietesExercice.filter(elem =>
      elem.id_copropriete_primaire === this.selectedCopropriete
      );
    // Select last exercice
    this.selectedExercice = this.listeExercice[this.listeExercice.length - 1].exercice ;
    // Search selected Copropriete
    this.setCoproprieteExercice();
  }

  onChangeExercice(event) {
    if (!this.selectedExercice) {
      return ;
    }
    // Search selected Copropriete
    this.setCoproprieteExercice();
  }

  setCoproprieteExercice () {

    if (this.listeCoproprietes.length == 0) {
      return ;
    }
      // Get Nom Copropriete
      const nomCopropriete = this.listeExercice[0].nom ;
      const idCopropriete = this.getIDCopropriete();

      const coproprieteExercice = {
            'copropriete_id' : idCopropriete ,
            'copropriete_primaire_nom' : nomCopropriete,
            'coporpriete_primaire_id': this.selectedCopropriete,
            'exercice': this.selectedExercice
            } ;
      this.globaleService.setCoproprieteExercice(coproprieteExercice);
  }

  defaultCoproprieteExercice () {

    if (this.listeCoproprietes.length == 0) {
      return ;
    }
    // Select last copropriete
    const lastCopropriete = this.listeCoproprietes[this.listeCoproprietes.length - 1]
                            .id_copropriete_primaire ;
    this.selectedCopropriete = lastCopropriete ;
    // Filter and search last exercice
    this.listeExercice = this.listeCoproprietesExercice.filter(elem =>
      elem.id_copropriete_primaire === this.selectedCopropriete
      );
    // Select last exercice
    this.selectedExercice = this.listeExercice[this.listeExercice.length - 1].exercice ;
  }

  getIDCopropriete () {

    if (!this.listeCoproprietesExercice) { return ; }

    // Filter listeCoproprietesExercice by selectedCopropriete (first filter 1)
    const filtreByCoproprietePrimaire = this.listeCoproprietesExercice.filter(elem =>
      elem.id_copropriete_primaire === this.selectedCopropriete
    );

    // Filter listeCoproprietesExercice by selectedExercice (seconde filter 2)
    const filtreByExercice  = filtreByCoproprietePrimaire.filter(elem =>
      elem.exercice === this.selectedExercice
    );

    return filtreByExercice[0].id_copropriete ;
  }

}
