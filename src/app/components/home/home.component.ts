import { Component, OnInit, OnDestroy } from '@angular/core';
import { GlobaleService, GlobalObject } from '../../services/globale.service';
import {Router, ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  /*
  * Globale variable
  */
  globalObject: GlobalObject = new GlobalObject() ;
  private globalObjectSubscription ;

  constructor(private globaleService: GlobaleService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
    this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject().subscribe(res => this.globalObject = res) ;
  }

  ngOnInit() {
    this.setGlobalObject() ;
  }

  ngOnDestroy () {
    this.globalObjectSubscription.unsubscribe();
  }

  setGlobalObject(): void {

    const globalObjectNew: GlobalObject = {
      'title': 'Acceuil',
      'breadcrumb': ['Bienvenue sur votre espace E-Syndic !']
     } ;
    const mergeObjects = {...this.globalObject, ...globalObjectNew} ;
    this.globaleService.setGlobalObject(mergeObjects);
  }
}
