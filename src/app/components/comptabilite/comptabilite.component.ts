import { Component, OnInit, OnDestroy } from '@angular/core';
import { GlobaleService, GlobalObject } from './../../services/globale.service';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-comptabilite',
  templateUrl: './comptabilite.component.html',
  styleUrls: ['./comptabilite.component.css']
})
export class ComptabiliteComponent implements OnInit, OnDestroy {

  /*
  * Globale variable
  */
 private globalObject: GlobalObject = new GlobalObject() ;
 private globalObjectSubscription ;

 constructor(private globaleService: GlobaleService,
             private router: Router,
             private activatedRoute: ActivatedRoute) {
         // Subscribe to globaleService
         this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject()
         .subscribe(globalObject => this.globalObject = globalObject) ;
  }

  ngOnInit() {
  }

  ngOnDestroy () {
    this.globalObjectSubscription.unsubscribe();
  }

}
