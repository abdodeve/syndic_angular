import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComptabiliteSidebarComponent } from './comptabilite-sidebar.component';

describe('ComptabiliteSidebarComponent', () => {
  let component: ComptabiliteSidebarComponent;
  let fixture: ComponentFixture<ComptabiliteSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComptabiliteSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComptabiliteSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
