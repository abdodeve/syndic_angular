import { Component, OnInit, OnDestroy } from '@angular/core';
import { GlobaleService, GlobalObject } from './../../../services/globale.service';

@Component({
  selector: 'app-comptabilite-sidebar',
  templateUrl: './comptabilite-sidebar.component.html',
  styleUrls: ['./comptabilite-sidebar.component.css']
})
export class ComptabiliteSidebarComponent implements OnInit, OnDestroy {

  /*
  * Globale variable
  */
  private globalObject: GlobalObject = new GlobalObject() ;
  private globalObjectSubscription ;

  constructor(private globaleService: GlobaleService) {
          // Subscribe to globaleService
          this.globalObjectSubscription = this.globaleService.get_Observable_GlobalObject()
          .subscribe(globalObject => this.globalObject = globalObject) ;
   }

  ngOnInit() {
  }

  ngOnDestroy () {
    this.globalObjectSubscription.unsubscribe();
  }

}
