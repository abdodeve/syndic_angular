import { Component, OnInit, AfterViewInit, OnDestroy} from '@angular/core';
import { GlobaleService, GlobalObject } from '../../../services/globale.service';
import { LoginServiceService } from '../../login/service/login-service.service' ;
// import 'custombox';
declare var Custombox: any ;



// reference to jQuery
declare var $: any;

@Component({
  selector: 'app-global-sidebar',
  templateUrl: './global-sidebar.component.html',
  styleUrls: ['./global-sidebar.component.css']
})
export class GlobalSidebarComponent implements OnInit, AfterViewInit, OnDestroy {

  private globalObject: GlobalObject = new GlobalObject() ;
  private globalObjectSubscription ;
  private coproprieteExerciceSubscription ;
  private copropriete ;
  private exercice ;
  private userLoggedIn ;

  constructor( private globaleService: GlobaleService,
               private loginServiceService: LoginServiceService ) {
        // Subscribe to globaleService (globalObject)
      this.globalObjectSubscription = this.globaleService.
           get_Observable_GlobalObject().subscribe(globalObject => this.globalObject = globalObject) ;
      this.coproprieteExerciceSubscription = this.globaleService.get_Observable_CoproprieteExercice().subscribe(res => {
        if (!res) {
          return ;
        }
        this.copropriete = res.copropriete_primaire_nom ;
        this.exercice = res.exercice ;
      });
   }

  ngOnInit() {
    this.getUserLoggedIn();
  }

  ngOnDestroy () {
    this.globalObjectSubscription.unsubscribe();
    this.coproprieteExerciceSubscription.unsubscribe();
  }

  getUserLoggedIn(): void {
    this.loginServiceService.userLoggedIn().subscribe(res => {
          this.globaleService.setGlobalObject({ ...this.globalObject, 
                                                ...{
                                                    'userLoggedIn': res.user.name,
                                                    'user_id':  res.user.id
                                                   }
                                              });   
          this.userLoggedIn = this.globalObject.userLoggedIn ;
    } ) ;
  }

  ngAfterViewInit() {
    // this.initSlimscrollMenu();
    // this.initMetisMenu();
  }

  // initSlimscrollMenu() {
  //   $('.slimscroll-menu').slimscroll({
  //       height: 'auto',
  //       position: 'right',
  //       size: '8px',
  //       color: '#9ea5ab',
  //       wheelStep: 10,
  //       touchScrollStep : 75
  //   });
  // }

  //  initMetisMenu() {
  //   // metis menu
  //   $('#side-menu').metisMenu();
  // }

}
