import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RapportSidebarComponent } from './rapport-sidebar.component';

describe('RapportSidebarComponent', () => {
  let component: RapportSidebarComponent;
  let fixture: ComponentFixture<RapportSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RapportSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RapportSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
