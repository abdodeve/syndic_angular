import { Component, OnInit, AfterViewInit, OnDestroy} from '@angular/core';
import { GlobaleService, GlobalObject } from '../../../services/globale.service';

// reference to jQuery
declare var $: any;

@Component({
  selector: 'app-rapport-sidebar',
  templateUrl: './rapport-sidebar.component.html',
  styleUrls: ['./rapport-sidebar.component.css']
})
export class RapportSidebarComponent  implements OnInit, AfterViewInit, OnDestroy {

  private globalObject: GlobalObject = new GlobalObject() ;
  private globalObjectSubscription ;

  constructor( private globaleService: GlobaleService ) {
        // Subscribe to globaleService (globalObject)
        this.globalObjectSubscription = this.globaleService.
                                        get_Observable_GlobalObject().
                                        subscribe(globalObject => this.globalObject = globalObject) ;
   }

  ngOnInit() {
  }

  ngOnDestroy () {
    this.globalObjectSubscription.unsubscribe();
  }

  ngAfterViewInit() {
    $('#side-menu').metisMenu();
  }

}
