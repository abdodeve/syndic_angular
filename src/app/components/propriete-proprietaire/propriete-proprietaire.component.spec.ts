import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProprieteProprietaireComponent } from './propriete-proprietaire.component';

describe('ProprieteProprietaireComponent', () => {
  let component: ProprieteProprietaireComponent;
  let fixture: ComponentFixture<ProprieteProprietaireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProprieteProprietaireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProprieteProprietaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
