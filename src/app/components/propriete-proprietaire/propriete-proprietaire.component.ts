import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { GridApi, ColumnApi, Events } from 'ag-grid';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { GlobaleService, GlobalObject } from '../../services/globale.service';
import { GridClass } from '../../services/grid.class' ;
import { NgxPermissionsService } from 'ngx-permissions' ;


@Component({
  selector: 'app-propriete-proprietaire',
  templateUrl: './propriete-proprietaire.component.html',
  styleUrls: ['./propriete-proprietaire.component.css']
})
export class ProprieteProprietaireComponent implements OnInit {

  @Input() parentParams ;

  
  /*
  * Globale variables
  */
 private globalObject: GlobalObject = new GlobalObject() ;
 private columnDefs = [
   {headerName: 'Proprietai', field: 'nom', checkboxSelection: true },
   {headerName: 'Prenom', field: 'prenom' },
   {headerName: 'Titre', field: 'titre'},
   {
     headerName: 'Action',
     suppressMenu: true,
     suppressSorting: true,
     width: 130,
     template: `
               <button type="button" data-action-type="edit"
                 class="btn btn-icon waves-effect waves-light btn-info" title="Editer">
                 <i class="fa fa-edit" data-action-type="edit"></i>
               </button>

               <button type="button" data-action-type="delete"
                 class="btn btn-icon waves-effect waves-light btn-danger" title="Supprimer">
                 <i class="fa fa-trash" data-action-type="delete"></i>
               </button>
               `
   }
 ];

  private gridApi: GridApi ;
  private gridColumnApi: ColumnApi ;
  private isColFit = true ;

  constructor(private globaleService: GlobaleService,
    private snotifyService: SnotifyService,
    private spinnerService: Ng4LoadingSpinnerService,
    private gridClass: GridClass,
    private permissionsService: NgxPermissionsService)  
    {

    }

  ngOnInit() {
    console.log(this.parentParams);
  }

}
