import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MyHttpInterceptor } from './services/my-http-interceptor' ;
import { SnotifyModule, SnotifyService, ToastDefaults } from 'ng-snotify';
import { AgGridModule } from 'ag-grid-angular';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { NgSelectModule } from '@ng-select/ng-select';
import { UiSwitchModule } from 'ngx-ui-switch';
import { NgxPermissionsModule } from 'ngx-permissions';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { FileSaverModule } from 'ngx-filesaver';



  //
 // Import Components
import { AppComponent } from './app.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { TopbarComponent } from './components/topbar/topbar.component';
import { HomeComponent } from './components/home/home.component';
import { ListeProprietaireComponent } from './components/proprietaire/liste-proprietaire/liste-proprietaire.component';
import { MajProprietaireComponent } from './components/proprietaire/maj-proprietaire/maj-proprietaire.component';
import { MajProprieteComponent } from './components/propriete/maj-propriete/maj-propriete.component';
import { ListeProprieteComponent } from './components/propriete/liste-propriete/liste-propriete.component';
import { LoginComponent } from './components/login/login.component';

  //
 // Import Services
import { GlobaleService } from './services/globale.service';
import { ProprietaireServiceService } from './components/proprietaire/service/proprietaire-service.service';
import { LoginServiceService } from './components/login/service/login-service.service';
import { AuthGuardService } from './services/guards/auth-guard.service';
import { ChangePasswordComponent } from './components/user/change-password/change-password.component';
import { ResetPassowrdComponent } from './components/user/reset-passowrd/reset-passowrd.component';
import { ComptabiliteSidebarComponent } from './components/sidebar/comptabilite-sidebar/comptabilite-sidebar.component';
import { ComptabiliteComponent } from './components/comptabilite/comptabilite.component';
import { FactureComponent } from './components/comptabilite/facture/facture.component';
import { HomeSidebarComponent } from './components/sidebar/home-sidebar/home-sidebar.component';
import { SideGuard } from './services/guards/side/side.guard';
import { RapportSidebarComponent } from './components/sidebar/rapport-sidebar/rapport-sidebar.component';
import { RapportComponent } from './components/rapport/rapport.component';
import { RapportComptabiliteComponent } from './components/rapport/rapport-comptabilite/rapport-comptabilite.component';
import { GlobalSidebarComponent } from './components/sidebar/global-sidebar/global-sidebar.component';
import { TrashProprietaireComponent } from './components/proprietaire/trash-proprietaire/trash-proprietaire.component';
import { ModalCopExerComponent } from './components/topbar/modal-cop-exer/modal-cop-exer.component';
import { ListPermissionsComponent } from './components/permissions-roles/list-permissions/list-permissions.component';
import { ListeUserComponent } from './components/user/liste-user/liste-user.component';
import { MajUserComponent } from './components/user/maj-user/maj-user.component';
import { TopbarService } from './components/topbar/service/topbar.service';
import { ListeActivityLogComponent } from './components/activity-log/liste-activity-log/liste-activity-log.component';
import { TrashProprieteComponent } from './components/propriete/trash-propriete/trash-propriete.component';
import { RegisterModalComponent } from './components/user/register-modal/register-modal.component';
import { ProprieteProprietaireComponent } from './components/propriete-proprietaire/propriete-proprietaire.component';
import { AssignedProprieteComponent } from './components/proprietaire/assigned-propriete/assigned-propriete.component';

  //
 // Setup Routes
const appRoutes: Routes = [
  { path: 'login',      component: LoginComponent },
  // Interfaces Default
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home' , canActivate: [SideGuard], children: [
        { path: '', component: HomeComponent, canActivate: [AuthGuardService] },
        { path: 'permissions-roles', children: [
          { path: '', component: ListPermissionsComponent, canActivate: [AuthGuardService] },
          { path: 'liste', component: ListPermissionsComponent, canActivate: [AuthGuardService] },
        ]},
        { path: 'user', children: [
          { path: '', component: ListeUserComponent, canActivate: [AuthGuardService] },
          { path: 'liste', component: ListeUserComponent, canActivate: [AuthGuardService]},
          { path: 'new', component: MajUserComponent, canActivate: [AuthGuardService] },
          { path: 'edit/:id', component: MajUserComponent, canActivate: [AuthGuardService] }
        ]},
        { path: 'activity-log', children: [
          { path: 'liste', component: ListeActivityLogComponent, canActivate: [AuthGuardService]}
        ]},
        { path: 'proprietaire', children: [
          { path: '', component: ListeProprietaireComponent, canActivate: [AuthGuardService]},
          { path: 'liste', component: ListeProprietaireComponent,
            canActivate: [AuthGuardService, NgxPermissionsGuard],
            data: {permissions: { only: ['display_proprietaire'], redirectTo: '/home' }}
          },
          { path: 'new', component: MajProprietaireComponent,
            canActivate: [AuthGuardService, NgxPermissionsGuard],
            data: {permissions: { only: ['create_proprietaire'], redirectTo: '/home' }}
          },
          { path: 'edit/:id', component: MajProprietaireComponent,             
            canActivate: [AuthGuardService, NgxPermissionsGuard],
            data: {permissions: { only: ['display_proprietaire'], redirectTo: '/home' }}
          },
          { path: 'trash', component: TrashProprietaireComponent,
            canActivate: [AuthGuardService, NgxPermissionsGuard],
            data: {permissions: { only: ['restore'], redirectTo: '/home' }}
          },
        ]},
        { path: 'propriete', children: [
          { path: '', component: ListeProprieteComponent, canActivate: [AuthGuardService]},
          { path: 'liste', component: ListeProprieteComponent,
            canActivate: [AuthGuardService, NgxPermissionsGuard],
            data: {permissions: { only: ['display_propriete'], redirectTo: '/home' }}
          },
          { path: 'new', component: MajProprieteComponent,
            canActivate: [AuthGuardService, NgxPermissionsGuard],
            data: {permissions: { only: ['create_propriete'], redirectTo: '/home' }}
          },
          { path: 'edit/:id', component: MajProprieteComponent,             
            canActivate: [AuthGuardService, NgxPermissionsGuard],
            data: {permissions: { only: ['display_propriete'], redirectTo: '/home' }}
          },
          { path: 'trash', component: TrashProprieteComponent,
            canActivate: [AuthGuardService, NgxPermissionsGuard],
            data: {permissions: { only: ['restore'], redirectTo: '/home' }}
          },
        ]}
      ]
  },
  // Interfaces Comptabilite
  { path: 'comptabilite' , canActivate: [SideGuard],
      children: [
        { path: '', component: ComptabiliteComponent },
        { path: 'facture', component: FactureComponent }
    ]
  },
  // Side Rapport
    { path: 'rapport' , canActivate: [SideGuard],
    children: [
      { path: '', component: RapportComponent },
      { path: 'comptabilite', component: RapportComptabiliteComponent }
      ]
    },
  { path: '**', component: HomeComponent},
];


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    TopbarComponent,
    HomeComponent,
    ListeProprietaireComponent,
    MajProprietaireComponent,
    MajProprieteComponent,
    ListeProprieteComponent,
    LoginComponent,
    ChangePasswordComponent,
    ResetPassowrdComponent,
    ComptabiliteSidebarComponent,
    ComptabiliteComponent,
    FactureComponent,
    HomeSidebarComponent,
    RapportSidebarComponent,
    RapportComponent,
    RapportComptabiliteComponent,
    GlobalSidebarComponent,
    TrashProprietaireComponent,
    ModalCopExerComponent,
    ListPermissionsComponent,
    ListeUserComponent,
    MajUserComponent,
    ListeActivityLogComponent,
    TrashProprieteComponent,
    RegisterModalComponent,
    ProprieteProprietaireComponent,
    AssignedProprieteComponent,
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserModule,
    SnotifyModule,
    AgGridModule.withComponents([]),
    Ng4LoadingSpinnerModule.forRoot(),
    NgSelectModule,
    UiSwitchModule,
    NgxPermissionsModule.forRoot(),
    LoadingBarHttpClientModule,
    FileSaverModule,
  ],
  providers: [GlobaleService, ProprietaireServiceService, LoginServiceService,
              {
                provide: HTTP_INTERCEPTORS,
                useClass: MyHttpInterceptor,
                multi: true
              },
               AuthGuardService,
               SideGuard,
              { provide: 'SnotifyToastConfig', useValue: ToastDefaults},
              SnotifyService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
