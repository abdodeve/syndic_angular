 /*
    /* Code below added by MarocGeek Team
    */
$("document").ready(function(){
//Launch full screen
function launchIntoFullscreen(element) {
    var isFull = false ;
    if(element.requestFullscreen) {
     element.requestFullscreen();
     isFull = true ;
    } else if(element.mozRequestFullScreen) {
     element.mozRequestFullScreen();
     isFull = true ;
    } else if(element.webkitRequestFullscreen) {
     element.webkitRequestFullscreen();
     isFull = true ;
    } else if(element.msRequestFullscreen) {
     element.msRequestFullscreen();
     isFull = true ;
    }
    if(isFull){
     exitFullscreen();
    }
    }
    
    //Exit full screen
    function exitFullscreen() {
    if(document.exitFullscreen) {
     document.exitFullscreen();
    } else if(document.mozCancelFullScreen) {
     document.mozCancelFullScreen();
    } else if(document.webkitExitFullscreen) {
     document.webkitExitFullscreen();
    }
    }
    //Click Full Screen event
    $( "#fullscreen" ).click(function() {
        launchIntoFullscreen(document.documentElement);
      });

    
      /**/
     /** Include Jquery.app.js /*/
    /**/

// initLeftMenuCollapse
    $('.button-menu-mobile').on('click', function (event) {
        event.preventDefault();
        $('body').toggleClass('enlarged');
        // initSlimscrollMenu
        $('.slimscroll-menu').slimscroll({
        height: 'auto',
        position: 'right',
        size: '8px',
        color: '#9ea5ab',
        wheelStep: 5
        });
     
    });

// initSlimscroll
     $('.slimscroll').slimscroll({
        height: 'auto',
        position: 'right',
        size: '8px',
        color: '#9ea5ab'
    });

//initMetisMenu
    $("#side-menu").metisMenu();

//initEnlarge
    if ($(window).width() < 1025) {
        $('body').addClass('enlarged');
    } else {
        if ($('body').data('keep-enlarged') != true)
            $('body').removeClass('enlarged');
    }

//initActiveMenu
    // === following js will activate the menu in left side bar based on url ====
    $("#sidebar-menu a").each(function () {
        if (this.href == window.location.href) {
            $(this).addClass("active");
            $(this).parent().addClass("active"); // add active to li of the current link
            $(this).parent().parent().addClass("in");
            $(this).parent().parent().prev().addClass("active"); // add active class to an anchor
            $(this).parent().parent().parent().addClass("active");
            $(this).parent().parent().parent().parent().addClass("in"); // add active to li of the current link
            $(this).parent().parent().parent().parent().parent().addClass("active");
        }
    });
   
    //Switcher init
     var elem = document.querySelector('.js-switch');
     if(elem)
     var init = new Switchery(elem);


    /*
    *   Remove class active 
    *   while change tab-pane
    */
    $(document).on('click', '.btn-nav-removeClass', function( event ){
        $(event.target).removeClass('active');
    });

    /*
    *   DropZone
    */
    var message = "Faites glisser vos fichiers ici ou cliquez dans cette zone." ;
    // OnFileChange
    $(document).on('change', '.drag-nd-drop-upload .upload-file', function(e) {
        if( this.files.length != 0 ){
            $('.drag-nd-drop-upload p').text(this.files[0].name);
            return ;
        } 
        $('.drag-nd-drop-upload p').text(message);
    });
    // OnUpload 
    $(document).on('click', '.btn-upload-mg', function(e) {  
        $('.drag-nd-drop-upload p').text(message);
    });

      

}); //End (document).ready